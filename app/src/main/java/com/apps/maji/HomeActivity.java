package com.apps.maji;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.Adapter.GuruAdapter;
import com.apps.maji.Fragment.ChatsFragment;
import com.apps.maji.Fragment.HomeFragment;
import com.apps.maji.Fragment.MengajiFragment;
import com.apps.maji.Fragment.MoreFragment;
import com.apps.maji.Fragment.NilaiFragment;
import com.apps.maji.Fragment.RiwayatFragment;
import com.apps.maji.Model.ChatAddUserResponse;
import com.apps.maji.Model.ChatGroupCreateResponse;
import com.apps.maji.Model.ChatLoginResponse;
import com.apps.maji.Model.ChatUnreadMsg;
import com.apps.maji.Model.CheckTutorResponse;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataGuru;
import com.apps.maji.Model.DataProfile;
import com.apps.maji.Model.DetailTeacher;
import com.apps.maji.Model.ListGuruResponse;
import com.apps.maji.Model.ProfileResponse;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Model.UserCreateResponse;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.Constant;
import com.apps.maji.Utils.MyApplication;
import com.apps.maji.Utils.SessionManager;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.livinglifetechway.quickpermissions.annotations.WithPermissions;
import com.nex3z.notificationbadge.NotificationBadge;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.Constant.QRID;
import static com.apps.maji.Utils.Constant.QRREGISTER;
import static com.apps.maji.Utils.Method.getPickImageChooserIntent;
import static com.apps.maji.Utils.Method.isValidEmail;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";
    public FragmentManager fm = getSupportFragmentManager();

    Fragment fragment1 = new HomeFragment();
    Fragment fragment2 = new MengajiFragment();
    Fragment fragment3 = new RiwayatFragment();
    Fragment fragment4 = new NilaiFragment();
    Fragment fragment5 = new MoreFragment();
    Fragment fragment6 = new ChatsFragment();
    Fragment active = fragment1;
    Fragment activeShow = fragment1;

    ImageView imgHome, imgMengaji, imgRiyawat, imgNilai, imgMore;

    private final int PICK_IMAGE_REQUEST = 71;
    private final int SOUND_REQUEST = 72;
    private int requestCodez = 101, setEmail = 0;

    public SessionManager sessionManager;
    public String sToken, idUser, sNama, sEmail, sUserType;
    public Data myData;
    public TextView tvTitle, tvTopMsg;
    public View llHead, llTitle;
    public ImageView imgH;

    public ApiConfig restApi;
    public ApiConfig restApiChat;
    GuruAdapter guruAdapter;
    AlertDialog alertDialog;
    DataGuru dataGuru;
    public Boolean isConnected = true;
    public Socket mSocket;
    public DetailTeacher detailTeacher;
    public BottomNavigationView bottomNavigationView;
    NotificationBadge badge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
        sessionManager = new SessionManager(this);

        restApi = AppConfig.getRetrofit().create(ApiConfig.class);
        restApiChat = AppConfig.getRetrofitChat().create(ApiConfig.class);
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
        sNama = myData.getName();
        sEmail = myData.getEmail();
        sUserType = myData.getUsertype();

        if (!sessionManager.isTokenFCM())
            updateFCM();

        getMyProfile();
        checkMyTutor();

        Log.w(TAG, "onCreate: " + sNama);
//        initViews();
        fm.beginTransaction().add(R.id.frame_main, fragment6, "6").hide(fragment6).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment5, "5").hide(fragment5).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment4, "4").hide(fragment4).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment1, "1").commit();
//
//        methodWithPermissions();
//        imgChange(1, 0, 0, 0, 0);
        initViews();
    }

    void initViews() {
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                onMenuClick(item);
                return true;
            }
        });

        llHead = findViewById(R.id.llHead);
        tvTitle = findViewById(R.id.tvTitle);
        tvTopMsg = findViewById(R.id.tvTopMsg);
        imgH = findViewById(R.id.imgH);

        GlideApp.with(this)
                .load(R.drawable.headh)
                .into(imgH);

        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/volterounded_semibold.otf");
        badge = findViewById(R.id.badges);
        badge.getTextView().setTypeface(face);
    }

    void initChat() {

        chatCreate();

        MyApplication app = (MyApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Constant.CHAT_MESSAGE, onChatMessage);
        mSocket.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (fm.findFragmentByTag("5").isVisible()) {
            getMyProfile();
        }
    }

    @Override
    public void onBackPressed() {
        if (fm.findFragmentByTag("2").isVisible()) {
            ((MengajiFragment) active).doBack();
        } else if (fm.findFragmentByTag("3").isVisible()) {
            ((RiwayatFragment) active).doBack();
        } else if (fm.findFragmentByTag("4").isVisible()) {
            ((NilaiFragment) active).doBack();
        } else if (fm.findFragmentByTag("5").isVisible()) {
            ((MoreFragment) active).doBack();
        } else if (fm.findFragmentByTag("6").isVisible()) {
            ((ChatsFragment) active).doBack();
        } else
            super.onBackPressed();
    }

    public void doMenuFirst() {

        bottomNavigationView.setSelectedItemId(R.id.llHome);
        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).show(fragment1).commit();
        active = fragment1;
        imgChange(1, 0, 0, 0, 0);
    }

    public void doHideChat() {
        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).show(activeShow).commit();
        active = activeShow;
        bottomNavigationView.setVisibility(View.VISIBLE);
        llHead.setVisibility(View.VISIBLE);
        chatUnread();
    }

    public void onMenuClick(MenuItem v) {
        if (v.getItemId() == R.id.llHome) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment1).commit();
            active = fragment1;
            ((HomeFragment) fragment1).getRaport(false);
            imgChange(1, 0, 0, 0, 0);

        } else if (v.getItemId() == R.id.llMengaji) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment2).commit();
            active = fragment2;
            ((MengajiFragment) fragment2).checkTutor();
            imgChange(0, 1, 0, 0, 0);

        } else if (v.getItemId() == R.id.llRiwayat) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment3).commit();
            active = fragment3;
            ((RiwayatFragment) fragment3).getRaport(false);
            imgChange(0, 0, 1, 0, 0);

        } else if (v.getItemId() == R.id.llNilai) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment4).commit();
            active = fragment4;
            ((NilaiFragment) fragment4).getRaport(false);
            imgChange(0, 0, 0, 1, 0);

        } else if (v.getItemId() == R.id.llMore) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment5).commit();
            active = fragment5;
            imgChange(0, 0, 0, 0, 1);
            ((MoreFragment) fragment5).setInfo();
            ((MoreFragment) fragment5).setTutor();

        }
//        else if (v.getItemId() == R.id.llMore) {
//            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
//            fm.beginTransaction().show(fragment6).commit();
//            active = fragment6;
//            imgChange(0, 0, 0, 0, 1);
//            ((ChatsFragment) fragment6).onReceive();
//
//        }
    }

    public void doChat(View v) {
        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
        fm.beginTransaction().show(fragment6).commit();
        active = fragment6;
        ((ChatsFragment) fragment6).onUnread();
        bottomNavigationView.setVisibility(View.GONE);
        llHead.setVisibility(View.GONE);

    }

    void imgChange(int a, int b, int c, int d, int e) {
//        animateImageView(imgHome, a);
//        animateImageView(imgMengaji, b);
//        animateImageView(imgRiyawat, c);
//        animateImageView(imgNilai, d);
//        animateImageView(imgMore, e);
        if (a == 1) {
            activeShow = fragment1;
            tvTitle.setText("Beranda");
            tvTopMsg.setVisibility(View.VISIBLE);
            imgH.setVisibility(View.VISIBLE);
        } else {
            tvTopMsg.setVisibility(View.GONE);
            imgH.setVisibility(View.GONE);
        }
        if (b == 1) {
            activeShow = fragment2;
            tvTitle.setText("Mengaji");
        }
        if (c == 1) {
            activeShow = fragment3;
            tvTitle.setText("Riwayat");
        }
        if (d == 1) {
            activeShow = fragment4;
            tvTitle.setText("Nilai");
        }
        if (e == 1) {
            activeShow = fragment5;
            tvTitle.setText("Lainnya");
        }
    }

    public void onScanQR(View v) {
        startActivityForResult(new Intent(HomeActivity.this, QRScanActivity.class), QRREGISTER);
    }

    public void doRiwayat(View v){
        bottomNavigationView.setSelectedItemId(R.id.llRiwayat);
//        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
//        fm.beginTransaction().show(fragment3).commit();
//        active = fragment3;
//        ((RiwayatFragment) fragment3).getRaport(false);
//        imgChange(0, 0, 1, 0, 0);
    }

//    public void animateImageView(final ImageView v, int os) {
//        final int orange;
//        if (os == 1) {
//            orange = getResources().getColor(R.color.Yalloew);
//        } else {
//            orange = getResources().getColor(R.color.white);
//        }
//
//        final ValueAnimator colorAnim = ObjectAnimator.ofFloat(0f, 1f);
//        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                float mul = (Float) animation.getAnimatedValue();
//                int alphaOrange = adjustAlpha(orange, mul);
//                v.setColorFilter(alphaOrange, PorterDuff.Mode.SRC_ATOP);
//                if (mul == 0.0) {
//                    v.setColorFilter(null);
//                }
//            }
//        });
//
//        colorAnim.setDuration(100);
//        colorAnim.start();
//
//    }

    public void recordSound(View b) {

        Intent intent = new Intent(this, RecordingActivity.class);
        startActivityForResult(intent, SOUND_REQUEST);
    }

    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public void chooseImage(View v) {
        startActivityForResult(getPickImageChooserIntent(), PICK_IMAGE_REQUEST);
    }

    @WithPermissions(
            permissions = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
    )
    public void methodWithPermissions() {

    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QRREGISTER) {
            if (resultCode == RESULT_OK) {
                ((MengajiFragment) fragment2).setHasilScan(data.getStringExtra(QRID).trim());
            }
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap mBitmap;
                mBitmap = BitmapFactory.decodeFile(filePath);
                ((MengajiFragment) fragment2).getByteArrayInBackground(mBitmap);
                ((MengajiFragment) fragment2).setImage(mBitmap);

            }
        }

        if (requestCode == SOUND_REQUEST) {
            if (resultCode == RESULT_OK) {
                // Great! User has recorded and saved the audio file
                Toast.makeText(this, "Sound Recorded", Toast.LENGTH_SHORT).show();
                ((MengajiFragment) fragment2).setSound();
            } else if (resultCode == RESULT_CANCELED) {
                // Oops! User has canceled the recording
                Toast.makeText(this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void getMyProfile() {
        restApi.myProfile(myData.getId(), sToken).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                    if (response.body().isStatus()) {
                        if (response.body().getDataProfile() != null) {
                            DataProfile data = response.body().getDataProfile();
                            if (data.getUsertype().equals("student")) {
                                String serialize = data.serialize();
                                Log.i(TAG, "onResponse: Data SERVER " + data.getEmail());
                                sessionManager.createDataProfile(serialize);

                                String ds = sessionManager.getData();
                                Data d = Data.create(ds);
                                d.setName(data.getName());
                                d.setEmail(data.getEmail());
                                String serializeD = d.serialize();
                                sessionManager.createData(serializeD);

                                tvTopMsg.setText("Assalamualaikum, "+data.getName());

                                if (fm.findFragmentByTag("5").isVisible()) {
                                    ((MoreFragment) fragment5).setInfo();
                                }
                                if (data.getEmail().equals(""))
                                    showEmailDialog("", "");
                                else if (!data.getStatus().equals("1") && setEmail == 0)
                                    showEmailDialog(data.getEmail(), "Apakah alamat Email Anda sudah benar? atau seperti nya Anda belum mengaktifasi Akun Anda. Silahkan masukkan kembali email Anda");

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    void checkMyTutor() {
        restApi.checkMyTutor(myData.getId(), sToken).enqueue(new Callback<CheckTutorResponse>() {
            @Override
            public void onResponse(Call<CheckTutorResponse> call, Response<CheckTutorResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                    if (response.body().isStatus()) {
                        if (response.body().getDetailTeacher() != null) {
                            DetailTeacher detailTeacher = response.body().getDetailTeacher();
                            sessionManager.createDataMyTutor(detailTeacher.serialize());
                            initChat();
                        }
                    } else {
                        getGuru();
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckTutorResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                checkMyTutor();

            }
        });
    }

    void chatCreate() {
        restApiChat.chatCreate(myData.getId(), myData.getName(), myData.getName()).enqueue(new Callback<UserCreateResponse>() {
            @Override
            public void onResponse(Call<UserCreateResponse> call, Response<UserCreateResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        chatLogin();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCreateResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    void chatLogin() {
        restApiChat.chatLogin(myData.getId()).enqueue(new Callback<ChatLoginResponse>() {
            @Override
            public void onResponse(Call<ChatLoginResponse> call, Response<ChatLoginResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {
                            sessionManager.createTokenChat(response.body().getApiToken());
                            chatCreateGroup();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatLoginResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    void chatCreateGroup() {
        String dt = sessionManager.getDataMyTutor();
        if (dt != null) {
            detailTeacher = DetailTeacher.create(dt);
            Log.e(TAG, "chatCreateGroup: "+detailTeacher.getName() );
            restApiChat.chatCreateGroup(detailTeacher.getName(), detailTeacher.getId()).enqueue(new Callback<ChatGroupCreateResponse>() {
                @Override
                public void onResponse(Call<ChatGroupCreateResponse> call, Response<ChatGroupCreateResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: " + response.body().getMessage());
                            chatAddUser();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ChatGroupCreateResponse> call, Throwable t) {

                }
            });
        }
    }

    void chatAddUser() {
        restApiChat.chatAddtoGroup(detailTeacher.getId(), myData.getId()).enqueue(new Callback<ChatAddUserResponse>() {
            @Override
            public void onResponse(Call<ChatAddUserResponse> call, Response<ChatAddUserResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: chatAddUser " + response.body());
                        chatUnread();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatAddUserResponse> call, Throwable t) {

            }
        });
    }

    void chatUnread(){
        restApiChat.chatUnreadMsg(detailTeacher.getName(), myData.getId()).enqueue(new Callback<ChatUnreadMsg>() {
            @Override
            public void onResponse(Call<ChatUnreadMsg> call, Response<ChatUnreadMsg> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        Log.i(TAG, "onResponse: chatUnread "+response.body());
                        if (response.body().getMessageUnreads().size() > 0){
                            Log.i(TAG, "onResponse: chatUnread "+response.body().getMessageUnreads().toString());
                            if (response.body().getMessageUnreads().get(0).getUnread() > 0)
                                badge.setNumber(response.body().getMessageUnreads().get(0).getUnread(), true);
                            else badge.clear();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatUnreadMsg> call, Throwable t) {

            }
        });
    }

    public void showSpinerDialog(List<DataGuru> dataGurus) {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        View v = HomeActivity.this.getLayoutInflater().inflate(R.layout.dialog_choose_tutor, null);
        TextView rippleViewClose = v.findViewById(R.id.close);

        final EditText searchBox = v.findViewById(R.id.searchBox);

        guruAdapter = new GuruAdapter(this, dataGurus, new GuruAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DataGuru item) {
                showDialog(item.getName(), item.getId());
                dataGuru = item;
            }

            @Override
            public void onItemClick(DataGuru item, String w) {

            }
        });

        RecyclerView recyclerView = v.findViewById(R.id.rvTutorChoose);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(guruAdapter);

        adb.setView(v);
        alertDialog = adb.create();
        alertDialog.setCancelable(true);


        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                guruAdapter.getFilter().filter(searchBox.getText().toString());
            }
        });

        rippleViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Log.e(TAG, "onClick: CLICK");
            }
        });
        alertDialog.show();
    }

    public void getGuru() {
        restApi.getGuru(sToken).enqueue(new Callback<ListGuruResponse>() {
            @Override
            public void onResponse(Call<ListGuruResponse> call, Response<ListGuruResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    if (response.body().isStatus()) {
                        if (response.body().getDataGuru().size() > 0) {

                            showSpinerDialog(response.body().getDataGuru());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListGuruResponse> call, Throwable t) {

                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
            }
        });
    }

    public void showDialog(final String str, final String id) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Apakah kamu ingin memilih " + str + " sebagai guru Anda?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                followTutor(id);
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogs(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        builder.show();
    }

    public void showDialogss(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    private void showEmailDialog(String email, String msg) {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_email);
        dialog.setTitle("Maji");
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        TextView tvMsg = dialog.findViewById(R.id.tvForgots);
        if (msg.length() > 0)
            tvMsg.setText(msg);

        final EditText etEmail = dialog.findViewById(R.id.et_email);
        etEmail.setText(email);
        Button btnSet = dialog.findViewById(R.id.btnSet);
        Button btnSetCancel = dialog.findViewById(R.id.btnSetCancel);

        btnSetCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etEmail.getText().toString().equals("")) {
                    etEmail.requestFocus();
                    etEmail.setError("Mohon diisi!");
                } else if (!isValidEmail(etEmail.getText().toString())) {
                    etEmail.requestFocus();
                    etEmail.setError("Mohon diisi email Anda dengan Benar");
                } else {
                    restApi.setEmail(sToken, myData.getId(), etEmail.getText().toString()).enqueue(new Callback<ServerResponse>() {
                        @Override
                        public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                            if (response.body() != null) {
                                Log.d("Response", "= " + response.body());
                                if (response.body().isSuccess()) {
                                    dialog.hide();
                                    setEmail = 1;
                                    Toast.makeText(HomeActivity.this, "Berhasil mengeset Email", Toast.LENGTH_SHORT).show();
                                    getMyProfile();
                                } else {
                                    showDialogss(response.body().getMessage());
                                }
                            } else {
                                showDialogss("Terjadi kesalahan, silahkan coba lagi");
                            }
                        }

                        @Override
                        public void onFailure(Call<ServerResponse> call, Throwable t) {

                            Log.e("failure", "message = " + t.getMessage());
                            Log.e("failure", "cause = " + t.getCause());
                            showDialogss("Terjadi kesalahan, silahkan coba lagi");
                        }
                    });
                }
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void showDialogsz(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getGuru();
            }
        });
        builder.show();
    }

    void followTutor(String idt) {
        restApi.followTutor(sToken, myData.getId(), idt).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        String serialize = dataGuru.serialize();
                        sessionManager.createDataMyTutor(serialize);
                        ((MoreFragment) fragment5).setTutor();
                        showDialogs(response.body().getMessage());
                    } else {

                        showDialogss(response.body().getMessage());
                    }
                } else
                    showDialogss(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
                showDialogsz("Terjadi kesalahan, silahkan coba lagi");
            }
        });
    }

    void updateFCM() {
        restApi.sendFCM(myData.getId(), sessionManager.getTokenFCM(), sToken).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    // SOCKET EMIT

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "run: connected");
                    if (!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };

    private Emitter.Listener onChatMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (args.length > 0) {
                        Log.i(TAG, "run: " + args[0].toString());
                    }
                    Log.i(TAG, "run: chatMessage");
                    ((ChatsFragment) fragment6).onReceive();
                    chatUnread();
                }
            });
        }
    };
}
