package com.apps.maji.Utils;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Dell_Cleva on 18/02/2019.
 */

public class Method {

    private static final String TAG = "Method";

    public static String getDisplayableTime(String string_date)
    {
        long delta = 0;
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = f.parse(string_date);
            delta = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference=0;
        Long mDate = java.lang.System.currentTimeMillis();

        if(mDate > delta)
        {
            difference= mDate - delta;
            final long seconds = difference/1000;
            final long minutes = seconds/60;
            final long hours = minutes/60;
            final long days = hours/24;
            final long months = days/31;
            final long years = days/365;

            if (seconds < 0)
            {
                return "baru saja";
            }
            else if (seconds < 60)
            {
                return seconds == 1 ? "1 detik yang lalu" : seconds + " detik yang lalu";
            }
            else if (seconds < 120)
            {
                return "1 menit yang lalu";
            }
            else if (seconds < 2700) // 45 * 60
            {
                return minutes + " menit yang lalu";
            }
            else if (seconds < 5400) // 90 * 60
            {
                return "1 jam yang lalu";
            }
            else if (seconds < 86400) // 24 * 60 * 60
            {
                return hours + " jam yang lalu";
            }
            else if (seconds < 172800) // 48 * 60 * 60
            {
                return "kemarin";
            }
            else if (seconds < 2592000) // 30 * 24 * 60 * 60
            {
                return days + " hari yang lalu";
            }
            else if (seconds < 31104000) // 12 * 30 * 24 * 60 * 60
            {

                return months <= 1 ? "1 bulan yang lalu" : months + " bulan yang lalu";
            }
            else
            {

                return years <= 1 ? "1 tahun yang lalu" : years + " tahun yang lalu";
            }
        }
        return "";
    }

    public static String getUTCTime() {
        String sUTC;
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("Z");
        sUTC = date.format(currentLocalTime);
        return sUTC;
    }

    public static String getMonth() {
        String sUTC;
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"),
                Locale.getDefault());
        Date currentLocalTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("MMMM");
        sUTC = date.format(currentLocalTime);
        return sUTC;
    }

    public static String FormatDate(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdftimehour = new SimpleDateFormat("HH:mm");
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEEE, dd MMMM yyyy", new Locale("id"));
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date) + " " + sdftimehour.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatDateS(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdftimehour = new SimpleDateFormat("HH:mm");
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEEE, dd MMM yy", new Locale("id"));
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date) + " " + sdftimehour.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatDateOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEEE dd MMMM yyyy", new Locale("id"));
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatHariOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("EEEE");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatTglOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatTglBlnOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd MMM");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatBlnOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatThnOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatJamOnly(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat fmtOut = new SimpleDateFormat("HH:mm");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = fmtOut.format(date);
//        Log.e("F", kirim);
        return kirim;
    }

    public static String FormatHourChat(String a) {
        Date date = null;
        SimpleDateFormat sdfdate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdftimehour = new SimpleDateFormat("HH:mm");
        try {
            date = sdfdate.parse(a);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String kirim = sdftimehour.format(date);
//        Log.e("FormatHourChat", kirim);
        return kirim;
    }

    public static Intent getPickImageChooserIntent(@NonNull Context context) {
        return getPickImageChooserIntent(
                context, "Select Source", false, true);
    }

    public static String checkClass(String startTime, String endTime) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat FORMATTERSQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("id"));
        Date datenow = null, datestart = null, dateend = null;
        String a = "";
        String jamsekarang = FORMATTERSQL.format(c.getTime());
        try {

            datenow = FORMATTERSQL.parse(jamsekarang);
            datestart = FORMATTERSQL.parse(startTime);
            dateend = FORMATTERSQL.parse(endTime);

            if (datenow.compareTo(datestart) < 0) {
                a = "WAIT";
            } else if (datenow.compareTo(datestart) == 0 || datenow.compareTo(datestart) > 0 && datenow.compareTo(dateend) < 0) {
                a = "PLAY";
            } else if (datenow.compareTo(dateend) > 0) {
                a = "PASS";
            }

        } catch (ParseException e) {
            e.printStackTrace();
            a = "ERROR";
        }
        Log.i(TAG, "checkClass: "+a);
        return a;
    }

    public static Intent getPickImageChooserIntent(
            @NonNull Context context,
            CharSequence title,
            boolean includeDocuments,
            boolean includeCamera) {

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        // collect all camera intents if Camera permission is available
        if (!Method.isExplicitCameraPermissionRequired(context) && includeCamera) {
            allIntents.addAll(getCameraIntents(context, packageManager));
        }

        List<Intent> galleryIntents =
                getGalleryIntents(packageManager, Intent.ACTION_GET_CONTENT, includeDocuments);
        if (galleryIntents.size() == 0) {
            // if no intents found for get-content try pick intent action (Huawei P9).
            galleryIntents = getGalleryIntents(packageManager, Intent.ACTION_PICK, includeDocuments);
        }
        allIntents.addAll(galleryIntents);

        Intent target;
        if (allIntents.isEmpty()) {
            target = new Intent();
        } else {
            target = allIntents.get(allIntents.size() - 1);
            allIntents.remove(allIntents.size() - 1);
        }

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(target, title);

        // Add all other intents
        chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get all Camera intents for capturing image using device camera apps.
     */
    public static List<Intent> getCameraIntents(
            @NonNull Context context, @NonNull PackageManager packageManager) {

        List<Intent> allIntents = new ArrayList<>();

        // Determine Uri of camera image to  save.
        Uri outputFileUri = getCaptureImageOutputUri(context);

        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        return allIntents;
    }

    /**
     * Get all Gallery intents for getting image from one of the apps of the device that handle
     * images.
     */
    public static List<Intent> getGalleryIntents(
            @NonNull PackageManager packageManager, String action, boolean includeDocuments) {
        List<Intent> intents = new ArrayList<>();
        Intent galleryIntent =
                action == Intent.ACTION_GET_CONTENT
                        ? new Intent(action)
                        : new Intent(action, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            intents.add(intent);
        }

        // remove documents intent
        if (!includeDocuments) {
            for (Intent intent : intents) {
                if (intent
                        .getComponent()
                        .getClassName()
                        .equals("com.android.documentsui.DocumentsActivity")) {
                    intents.remove(intent);
                    break;
                }
            }
        }
        return intents;
    }

    /**
     * Check if explicetly requesting camera permission is required.<br>
     * It is required in Android Marshmellow and above if "CAMERA" permission is requested in the
     * manifest.<br>
     * See <a
     * href="http://stackoverflow.com/questions/32789027/android-m-camera-intent-permission-bug">StackOverflow
     * question</a>.
     */
    public static boolean isExplicitCameraPermissionRequired(@NonNull Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && hasPermissionInManifest(context, "android.permission.CAMERA")
                && context.checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Check if the app requests a specific permission in the manifest.
     *
     * @param permissionName the permission to check
     * @return true - the permission in requested in manifest, false - not.
     */
    public static boolean hasPermissionInManifest(
            @NonNull Context context, @NonNull String permissionName) {
        String packageName = context.getPackageName();
        try {
            PackageInfo packageInfo =
                    context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            final String[] declaredPermisisons = packageInfo.requestedPermissions;
            if (declaredPermisisons != null && declaredPermisisons.length > 0) {
                for (String p : declaredPermisisons) {
                    if (p.equalsIgnoreCase(permissionName)) {
                        return true;
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public static Uri getCaptureImageOutputUri(@NonNull Context context) {
        Log.i(TAG, "getCaptureImageOutputUri: ");
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent(Context)}.<br>
     * Will return the correct URI for camera and gallery image.
     *
     * @param context used to access Android APIs, like content resolve, it is your
     *                activity/fragment/widget.
     * @param data    the returned data of the activity result
     */
    public static Uri getPickImageResultUri(@NonNull Context context, @Nullable Intent data) {
        boolean isCamera = true;
        Log.i(TAG, "getPickImageResultUri: ");
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            Log.i(TAG, "getPickImageResultUri: " + action);
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera || data.getData() == null ? getCaptureImageOutputUri(context) : data.getData();
    }

    public static String checkVersion(Context context) {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert info != null;
        return info.versionName;
    }

    public static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
