package com.apps.maji.Utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.apps.maji.R;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Created by Dell_Cleva on 28/01/2019.
 */

public class MyApplication extends MultiDexApplication {

    private Typeface voltedFont;

    private Socket mSocket;
    {
        try {
//            mSocket = IO.socket(Constant.CHAT_SERVER_URL);
            IO.Options mOptions = new IO.Options();
            mOptions.query = "room=1";
            mSocket = IO.socket(Constant.CHAT_SERVER_URL, mOptions);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    @SuppressLint("StaticFieldLeak")
    public static SessionManager sessionManager;

    public static ApiConfig mAPIService;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void onCreate() {
        super.onCreate();
        sessionManager = new SessionManager(this);
        mAPIService = AppConfig.getRetrofit().create(ApiConfig.class);
//        voltedFont = Typeface.createFromAsset(getAssets(), R.font.VolteRounded_Bold);
    }

    public Typeface getVoltedFont() {
        return voltedFont;
    }
}