package com.apps.maji;

import android.content.DialogInterface;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.apps.maji.Model.Data;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.MyApplication;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.MyApplication.mAPIService;
import static com.apps.maji.Utils.MyApplication.sessionManager;

public class PasswordActivity extends AppCompatActivity {

    private static final String TAG = "PasswordActivity";

    TextInputEditText etPassL, etPassB, etPassBA;
    Button btnSave;
    public Data myData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        setTitle("Ganti Kata Sandi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String d = sessionManager.getData();
        myData = Data.create(d);
        initView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initView(){
        etPassL = findViewById(R.id.etPassLama);
        etPassB = findViewById(R.id.etPassBaru);
        etPassBA = findViewById(R.id.etPassBaruA);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePassword();
            }
        });
    }

    void savePassword(){
        if (etPassL.getText().length() == 0){
            etPassL.setError("Mohon di isi!");
            etPassL.requestFocus();

        } else if (etPassB.getText().length() == 0){
            etPassB.setError("Mohon di isi!");
            etPassB.requestFocus();

        } else if (etPassBA.getText().length() == 0){
            etPassBA.setError("Mohon di isi!");
            etPassBA.requestFocus();

        } else if (!etPassB.getText().toString().equals(etPassBA.getText().toString())){
            etPassBA.setError("Password tidak sesuai!");
            etPassBA.requestFocus();

        } else {
            mAPIService.setPassword(sessionManager.getToken(), myData.getId(), etPassL.getText().toString(), etPassBA.getText().toString()).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            if (response.body().isSuccess()){
                                showDialogs(response.body().getMessage());
                            } else {
                                showDialogss(response.body().getMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e("failure", "message = " + t.getMessage());
                    Log.e("failure", "cause = " + t.getCause());
                    showDialogss("Terjadi kesalahan, silahkan coba lagi");
                }
            });
        }
    }

    public void showDialogss(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogs(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }
}
