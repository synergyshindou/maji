package com.apps.maji.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.maji.Adapter.MyHomeAdapter;
import com.apps.maji.Adapter.MyHomeUploadAdapter;
import com.apps.maji.GlideApp;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataProfile;
import com.apps.maji.Model.DataTopMengaji;
import com.apps.maji.Model.Dataraport;
import com.apps.maji.Model.ListRaportResponse;
import com.apps.maji.Model.ListTopMengaji;
import com.apps.maji.R;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.apps.maji.Widget.SpacesItemDecoration;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    public TextView tvNo, tvTopMsg;
    SessionManager sessionManager;

    Data myData;

    ApiConfig getResponse;

    String sToken, idUser;

    RecyclerView recyclerView, rvUpload;
    MyHomeAdapter myHomeAdapter;
    MyHomeUploadAdapter myHomeUploadAdapter;
    List<Dataraport> dataraports = new ArrayList<>();
    List<DataTopMengaji> dataTopMengajis= new ArrayList<>();

    ProgressBar pb, pbUp;

    RoundedImageView roundedImageView;

    ImageView imgH;
    View llRaport;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        initView(v);

        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

        getRaport(true);
        getTopUpload(true);
        return v;
    }

    void initView(View v){

        tvNo = v.findViewById(R.id.tvnoconnect);
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRaport(true);
            }
        });

        pb = v.findViewById(R.id.pb_load);
        pbUp = v.findViewById(R.id.pb_upload);

        recyclerView = v.findViewById(R.id.rvHome);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        rvUpload = v.findViewById(R.id.rvUpload);
        rvUpload.addItemDecoration(new SpacesItemDecoration(20));

        roundedImageView = v.findViewById(R.id.imgPoster);

        GlideApp.with(getActivity())
                .load(R.drawable.bannermaji)
                .into(roundedImageView);

        llRaport = v.findViewById(R.id.llRaport);
    }

    public void getRaport(boolean isFirst){
        tvNo.setVisibility(View.GONE);
        if (isFirst)
            showProgress(true);

        getResponse.homeraport(sToken, idUser).enqueue(new Callback<ListRaportResponse>() {
            @Override
            public void onResponse(Call<ListRaportResponse> call, Response<ListRaportResponse> response) {
                Log.i(TAG, "onResponse: "+response.message());
                showProgress(false);
                if (response.body() != null){
                    Log.i(TAG, "onResponse: "+response.body());

                    if (response.body().isStatus()) {
                        dataraports = response.body().getDataraport();

                        if (dataraports != null && dataraports.size() > 0) {
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(horizontalLayoutManagaer);
                            myHomeAdapter = new MyHomeAdapter(getContext(), dataraports, new MyHomeAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(Dataraport item) {
                                    Log.i(TAG, "onItemClick: " + item.toString());
                                }
                            });
                            recyclerView.setAdapter(myHomeAdapter);
                            llRaport.setVisibility(View.VISIBLE);
                        } else {
                            llRaport.setVisibility(View.GONE);
                        }
                    } else {
                        llRaport.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ListRaportResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                tvNo.setVisibility(View.VISIBLE);
                showProgress(false);
            }
        });
    }

    public void getTopUpload(boolean isFirst){
        if (isFirst)
            showProgressUpload(true);

        getResponse.getTopMengaji(sToken, "5").enqueue(new Callback<ListTopMengaji>() {
            @Override
            public void onResponse(Call<ListTopMengaji> call, Response<ListTopMengaji> response) {
                Log.i(TAG, "onResponse: "+response.message());
                showProgressUpload(false);
                if (response.body() != null){
                    Log.i(TAG, "onResponse: "+response.body().toString());

                    if (response.body().isStatus()) {
                        dataTopMengajis = response.body().getDataTopMengaji();

                        if (dataTopMengajis != null && dataTopMengajis.size() > 0) {
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            rvUpload.setLayoutManager(horizontalLayoutManagaer);
                            myHomeUploadAdapter = new MyHomeUploadAdapter(getContext(), dataTopMengajis, new MyHomeUploadAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(DataTopMengaji item) {
                                    Log.i(TAG, "onItemClick: " + item.toString());
                                }
                            });
                            rvUpload.setAdapter(myHomeUploadAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListTopMengaji> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
//                tvNo.setVisibility(View.VISIBLE);
                showProgressUpload(false);
            }
        });
    }

    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showProgressUpload(final boolean show) {
        pbUp.setVisibility(show ? View.VISIBLE : View.GONE);
        rvUpload.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
