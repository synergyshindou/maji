package com.apps.maji.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.maji.Adapter.MyHomeAdapter;
import com.apps.maji.Adapter.NilaiAdapter;
import com.apps.maji.HomeActivity;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.Dataraport;
import com.apps.maji.Model.ListRaportResponse;
import com.apps.maji.R;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.apps.maji.Widget.SpacesItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NilaiFragment extends Fragment {


    private static final String TAG = "NilaiFragment";
    SessionManager sessionManager;

    Data myData;

    ApiConfig getResponse;

    String sToken, idUser;

    HomeActivity homeActivity;
    RecyclerView recyclerView;
    NilaiAdapter nilaiAdapter;
    List<Dataraport> dataraports = new ArrayList<>();
    ProgressBar pb;
    TextView tvNo;
    View llNo;

    public NilaiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
//        Log.i(TAG, "onCreate: "+sToken);
//        Log.i(TAG, "onCreate: "+idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nilai, container, false);
        initView(v);

        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

        getRaport(true);
        return v;
    }

    void initView(View v){

        tvNo = v.findViewById(R.id.tvnoconnect);
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRaport(true);
            }
        });

        pb = v.findViewById(R.id.pb_load);
        recyclerView = v.findViewById(R.id.rvNilai);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        llNo = v.findViewById(R.id.llNo);
    }

    public void getRaport(boolean isFirst){
        tvNo.setVisibility(View.GONE);
        if (isFirst)
            showProgress(true);

        getResponse.homeraport(sToken, idUser).enqueue(new Callback<ListRaportResponse>() {
            @Override
            public void onResponse(Call<ListRaportResponse> call, Response<ListRaportResponse> response) {
                Log.i(TAG, "onResponse: "+response.message());
                showProgress(false);
                if (response.body() != null){
                    Log.i(TAG, "onResponse: "+response.body());

                    if (response.body().isStatus()) {
                        dataraports = response.body().getDataraport();

                        if (dataraports != null && dataraports.size() > 0) {

                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(horizontalLayoutManagaer);
                            nilaiAdapter = new NilaiAdapter(getContext(), dataraports, new NilaiAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(Dataraport item) {
                                    Log.i(TAG, "onItemClick: " + item.toString());
                                }
                            });
                            recyclerView.setAdapter(nilaiAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            llNo.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            llNo.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        llNo.setVisibility(View.VISIBLE);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    llNo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ListRaportResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                tvNo.setVisibility(View.VISIBLE);
                showProgress(false);
            }
        });
    }

    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }


    public void doBack() {
        homeActivity.doMenuFirst();
    }
}
