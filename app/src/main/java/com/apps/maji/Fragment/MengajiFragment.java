package com.apps.maji.Fragment;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.Adapter.KelasAdapter;
import com.apps.maji.Classroom;
import com.apps.maji.HomeActivity;
import com.apps.maji.Model.DetailTeacher;
import com.apps.maji.Model.ListClas;
import com.apps.maji.Model.ListClassResponse;
import com.apps.maji.Model.ListGuruResponse;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.ProgressRequestBody;
import com.apps.maji.R;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.SessionManager;
import com.apps.maji.Widget.SpacesItemDecoration;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.Constant.CH_ID;
import static com.apps.maji.Utils.Method.checkClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class MengajiFragment extends Fragment implements ProgressRequestBody.UploadCallbacks {

    private static final String TAG = "MengajiFragment";
    public View llAwal, llMMandiri, llMOnline, llPlay, llMengaji, llOnline;
    public RadioGroup radioGroup;
    ImageView imgO, imgG;
    TextView tvHasil, tvPre, tvNoKelas, tvRun;
    Spinner spinGuru;
    MaterialSpinner spinGur;
    HomeActivity homeActivity;
    Button btnMengaji;
    String b64, sType = "", sGuruID = "", sSound = "";
    Bitmap myBitmap;
    String[] sTypes = {"mandiri", "guru", "online"}, sHasilScan;
    int back = 0, iRBSel = -1;
    ApiConfig restClient;
    List<String> list = new ArrayList<>();
    List<String> listId = new ArrayList<>();
    AlertDialog b, bC;
    AlertDialog.Builder dialogBuilder;
    RecyclerView recyclerView;
    KelasAdapter kelasAdapter;
    List<ListClas> listClas;
    View v;
    byte[] byteArray;
    private ImageView imageView;
    private AppCompatButton mButtonPlay;
    private AppCompatButton mButtonStop;
    private SeekBar mSeekBar;
    private TextView mPass;
    private TextView mDuration;
    private TextView mDue;
    private MediaPlayer mPlayer;
    private Handler mHandler;
    private Runnable mRunnable;
    ApiConfig getResponse;
    DetailTeacher detailTeacher;

    public MengajiFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_mengaji, container, false);

        homeActivity = (HomeActivity) getActivity();
        restClient = AppConfig.getRetrofit().create(ApiConfig.class);
        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
//        Log.w(TAG, "onCreateView: Data Tutor " + homeActivity.sessionManager.getDataMyTutor());
        if (homeActivity.sessionManager.getDataMyTutor() != null)
            detailTeacher = DetailTeacher.create(homeActivity.sessionManager.getDataMyTutor());

        InitView(v);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.i(TAG, "onCheckedChanged: " + checkedId);
//                if (checkedId == R.id.rbMandiri && back == 0) {
//                    if (detailTeacher != null) {
//                        iRBSel = checkedId;
//                        tvTitle.setText("Mengaji Mandiri");
//                        llAwal.setVisibility(View.GONE);
//                        llMMandiri.setVisibility(View.VISIBLE);
//                        sType = sTypes[0];
//                        back = 1;
//                    } else {
//                        showDialogss("Mohon pilih guru dimenu setting");
//                        back = 1;
//                    }
//                } else if (checkedId == R.id.rbGuru && back == 0) {
//                    if (detailTeacher != null) {
//                        iRBSel = checkedId;
//                        tvTitle.setText("Mengaji dengan Guru");
//                        llAwal.setVisibility(View.GONE);
//                        llMMandiri.setVisibility(View.VISIBLE);
//                        sType = sTypes[1];
//                        back = 1;
//                    } else {
//                        showDialogss("Mohon pilih guru dimenu setting");
//                        back = 1;
//                    }
//                } else if (checkedId == R.id.rbOnline && back == 0) {
//                    if (detailTeacher != null) {
//                        iRBSel = checkedId;
//                        tvTitle.setText("Mengaji Online");
//                        llAwal.setVisibility(View.GONE);
//                        llMOnline.setVisibility(View.VISIBLE);
//                        sType = sTypes[2];
//                        back = 2;
////                        getMyClass();
//                    } else {
//                        showDialogss("Mohon pilih guru dimenu setting");
//                        back = 1;
//                    }
//                } else {
//                    Log.w(TAG, "onCheckedChanged: YOUREdone");
//                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.fullimg);
                dialog.setCancelable(true);
                dialog.show();
                ImageView zoom = dialog.findViewById(R.id.imgzoom);
                zoom.setImageDrawable(imageView.getDrawable());
                zoom.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
            }
        });
        getGuru();
        return v;
    }

    void InitView(View v) {

        recyclerView = v.findViewById(R.id.rvHome);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        tvNoKelas = v.findViewById(R.id.tvNoKelas);
        tvPre = v.findViewById(R.id.tvPre);
        tvHasil = v.findViewById(R.id.tvHasilScan);
        radioGroup = v.findViewById(R.id.rgMengaji);
        llAwal = v.findViewById(R.id.llAwal);
        llMMandiri = v.findViewById(R.id.llMMandiri);
        llMOnline = v.findViewById(R.id.llMOnline);
        llPlay = v.findViewById(R.id.llPlaySound);
        spinGuru = v.findViewById(R.id.spinGuru);
        spinGur = v.findViewById(R.id.spinGur);
        imageView = v.findViewById(R.id.imgView);
        imgO = v.findViewById(R.id.imgO);
        imgG = v.findViewById(R.id.imgG);
        llMengaji = v.findViewById(R.id.llMengajiGuru);
        llMengaji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailTeacher != null) {
                    llMengaji.setBackground(homeActivity.getDrawable(R.drawable.rounded_blue));
                    llOnline.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
//                    iRBSel = checkedId;
//                    tvTitle.setText("Mengaji dengan Guru");
//                    llAwal.setVisibility(View.GONE);
                    llMMandiri.setVisibility(View.VISIBLE);
                    llMOnline.setVisibility(View.GONE);
                    imgO.setImageDrawable(homeActivity.getDrawable(R.drawable.monlineg));
                    imgG.setImageDrawable(homeActivity.getDrawable(R.drawable.mguru));
                    sType = sTypes[1];
                    back = 1;
                } else {
                    showDialogss("Mohon pilih guru dimenu setting");
                    back = 1;
                }
            }
        });
        llOnline = v.findViewById(R.id.llMengajiOnline);
        llOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailTeacher != null) {
                    llMengaji.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
                    llOnline.setBackground(homeActivity.getDrawable(R.drawable.rounded_blue));
//                    iRBSel = checkedId;
//                    tvTitle.setText("Mengaji Online");
//                    llAwal.setVisibility(View.GONE);
                    llMOnline.setVisibility(View.VISIBLE);
                    llMMandiri.setVisibility(View.GONE);
                    imgO.setImageDrawable(homeActivity.getDrawable(R.drawable.monline));
                    imgG.setImageDrawable(homeActivity.getDrawable(R.drawable.mgurug));
                    sType = sTypes[2];
                    back = 2;
//                        getMyClass();
                } else {
                    showDialogss("Mohon pilih guru dimenu setting");
                    back = 1;
                }
            }
        });

        btnMengaji = v.findViewById(R.id.btnMengaji);
        btnMengaji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doMengaji();
            }
        });

        tvRun = v.findViewById(R.id.tvTkno);
        tvRun.setSelected(true);

        // Get the widget reference from xml layout
        mButtonPlay = v.findViewById(R.id.btn_playa);
        mButtonStop = v.findViewById(R.id.btn_stopa);
        mSeekBar = v.findViewById(R.id.seek_bar);
        mPass = v.findViewById(R.id.tv_pass);
        mDuration = v.findViewById(R.id.tv_duration);
        mDue = v.findViewById(R.id.tv_due);

        // Initialize the handler
        mHandler = new Handler();

        // Click listener for playing button
        mButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // If media player another instance already running then stop it first
                stopPlaying();

                // Initialize media player
                mPlayer = new MediaPlayer();
                try {

                    String filePath = Environment.getExternalStorageDirectory() + "/Maji/Audios/majisounds.mp3";
                    mPlayer.setDataSource(filePath);
                    mPlayer.prepare();
                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            stopPlaying();
                        }

                    });
                    mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            // Initialize the seek bar
                            initializeSeekBar();
                            // Get the current audio stats
                            getAudioStats(0);
                            mSeekBar.setProgress(0);
                            // Start the media player
                            mPlayer.start();


                        }
                    });
                } catch (IOException e) {
                    Log.e("LOG_TAG", "prepare() failed");
                }

            }
        });

        // Set a click listener for top playing button
        mButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPlaying();
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mPlayer != null && b) {
                    Log.w(TAG, "onProgressChanged: " + i);
                    mPlayer.seekTo(i * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void setHasilScan(String a) {
        tvHasil.setText(a);
        tvHasil.setVisibility(View.VISIBLE);
        String[] spl = a.split("_");
        if (spl.length == 3) {
            sHasilScan = spl;
        } else if (spl.length > 3) {
            String[] sbar = a.split("--");
            String[] snew = new String[3];
            snew[1] = "";
            snew[2] = "";
            if (sbar.length > 0) {
                for (int i = 0; i < sbar.length; i++) {
                    Log.i(TAG, "setHasilScan: " + sbar[i]);
                    String[] s = sbar[i].split("_");
                    if (s.length > 2) {
                        snew[0] = s[0];
                        snew[1] += s[1] + ",";
                        snew[2] += s[2] + ",";
                    } else if (s.length == 2) {

                        snew[1] += s[0] + ",";
                        snew[2] += s[1] + ",";
                    }
                }
                for (int z = 0; z < snew.length; z++) {
                    Log.i(TAG, "setHasilScan: New " + z + " : " + snew[z]);
                }
            }
            sHasilScan = snew;
        } else {
            sHasilScan = spl;
        }
    }

    public void getByteArrayInBackground(final Bitmap mBitmap) {

        Thread thread = new Thread() {
            @Override
            public void run() {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
                byteArray = bos.toByteArray();
            }
        };
        thread.start();
    }

    public void setImage(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    public void setSound() {
        llPlay.setVisibility(View.VISIBLE);
        sSound = "1";
    }

    protected void stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
//            Toast.makeText(getContext(),"Stop playing.",Toast.LENGTH_SHORT).show();
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }
            mSeekBar.setProgress(0);
        }
    }

    protected void getAudioStats(int i) {
        int duration = mPlayer.getDuration() / 1000; // In milliseconds
        int due = (mPlayer.getDuration() - mPlayer.getCurrentPosition()) / 1000;
        int pass = duration - due;

        mPass.setText("" + pass + " seconds");
        mDuration.setText("" + duration + " seconds");
        mDue.setText("" + due + " seconds");
        Log.i(TAG, "getAudioStats: " + due + " | " + pass);
        Log.i(TAG, "getAudioStats: " + i);
    }

    protected void initializeSeekBar() {
        mSeekBar.setMax(mPlayer.getDuration() / 1000);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlayer != null) {
                    int mCurrentPosition = mPlayer.getCurrentPosition() / 1000; // In milliseconds
                    mSeekBar.setProgress(mCurrentPosition);
                    getAudioStats(mCurrentPosition);
                }
                mHandler.postDelayed(mRunnable, 1000);
            }
        };
        mHandler.postDelayed(mRunnable, 1000);
    }

    public void checkTutor() {
        if (homeActivity.sessionManager.getDataMyTutor() != null)
            detailTeacher = DetailTeacher.create(homeActivity.sessionManager.getDataMyTutor());
    }

    public void doMengaji() {
//        if (sGuruID.equals("")){
//            spinGur.setError("Mohon Pilih Guru");
//        } else
        if (detailTeacher != null) {
            sGuruID = detailTeacher.getId();
            if (sHasilScan == null) {
                Toast.makeText(homeActivity, "Mohon Scan barcode terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else if (sHasilScan.length != 3) {
                Toast.makeText(homeActivity, "Barcode tidak sesuai", Toast.LENGTH_SHORT).show();
            } else if (imageView.getDrawable() == null) {
                Toast.makeText(homeActivity, "Mohon ambil/upload gambar terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else if (sSound.equals("")) {
                Toast.makeText(homeActivity, "Mohon rekam suara terlebih dahulu", Toast.LENGTH_SHORT).show();
            } else {
                ShowProgressDialog();
                CheckMengaji();

            }
        } else {
            showDialogss("Mohon pilih guru dimenu setting");
        }
    }

    void sendToServer() {
        final BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
        myBitmap = drawable.getBitmap();

//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            byte[] ba = bytes.toByteArray();
//            b64 = Base64.encodeToString(ba, Base64.NO_WRAP);
        File file = new File(Environment.getExternalStorageDirectory() + "/Maji/Audios/majisounds.mp3");

        //Create request body with text description and text media type
        RequestBody id_user = RequestBody.create(MediaType.parse("multipart/form-data"), homeActivity.idUser);
        RequestBody id_tutor = RequestBody.create(MediaType.parse("multipart/form-data"), sGuruID);
        RequestBody hal = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[0]);
        RequestBody surat = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[1]);
        RequestBody ayat = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[2]);
        RequestBody page = RequestBody.create(MediaType.parse("multipart/form-data"), tvHasil.getText().toString());
//            RequestBody b64b = RequestBody.create(MediaType.parse("multipart/form-data"), b64);
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), sType);

        ProgressRequestBody fileBody = new ProgressRequestBody(file, this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), fileBody);

        File filei = createTempFile(myBitmap);
        ProgressRequestBody fileBodyi = new ProgressRequestBody(filei, this);
        MultipartBody.Part fileParti = MultipartBody.Part.createFormData("image", filei.getName(), fileBodyi);
        getResponse.mengaji(homeActivity.sToken, id_user, id_tutor, hal, surat, ayat, page, type, filePart, fileParti).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                HideProgressDialog();
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    Log.d("Response", "= " + response.body());
                    Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    if (response.body().isSuccess())
                        doBack();

                } else {
                    Toast.makeText(homeActivity, "Terjadi kesalahan coba lagi. . .", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Terjadi kesalahan coba lagi. . .", Toast.LENGTH_SHORT).show();
                HideProgressDialog();
            }

        });
    }

    void CheckMengaji() {

        //Create request body with text description and text media type
        RequestBody id_user = RequestBody.create(MediaType.parse("multipart/form-data"), homeActivity.idUser);
        RequestBody id_tutor = RequestBody.create(MediaType.parse("multipart/form-data"), sGuruID);
        RequestBody hal = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[0]);
        RequestBody surat = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[1]);
        RequestBody ayat = RequestBody.create(MediaType.parse("multipart/form-data"), sHasilScan[2]);
        RequestBody page = RequestBody.create(MediaType.parse("multipart/form-data"), tvHasil.getText().toString());
        RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), sType);

        getResponse.checkmengaji(homeActivity.sToken, id_user, id_tutor, hal, surat, ayat, page, type).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    Log.d("Response", "= " + response.body());
                    if (response.body().isSuccess()) {
                        tvPre.setText("Uploading data");

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // This method will be executed once the timer is over
                                // Start your app main activity
                                sendToServer();
                            }
                        }, 500);
                    } else {
                        HideProgressDialog();
                        showDialog("Anda sudah pernah mengaji halaman ini. Anda mau mengaji lagi di halaman yang sama ?");
                    }

                } else {
                    HideProgressDialog();
                    Toast.makeText(homeActivity, "Terjadi kesalahan coba lagi. . .", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Terjadi kesalahan coba lagi. . .", Toast.LENGTH_SHORT).show();
                HideProgressDialog();
            }

        });
    }

    void getGuru() {
        restClient.getGuru(homeActivity.sToken).enqueue(new Callback<ListGuruResponse>() {
            @Override
            public void onResponse(Call<ListGuruResponse> call, Response<ListGuruResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    if (response.body().isStatus()) {
                        if (response.body().getDataGuru().size() > 0) {
                            list = new ArrayList<>();
                            listId = new ArrayList<>();
                            for (int i = 0; i < response.body().getDataGuru().size(); i++) {
                                list.add(response.body().getDataGuru().get(i).getName());
                                listId.add(response.body().getDataGuru().get(i).getId());
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(homeActivity,
                                    R.layout.spinner_item, list);

                            ArrayAdapter<String> adapter = new ArrayAdapter<>(homeActivity, android.R.layout.simple_spinner_item, list);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinGur.setAdapter(adapter);
                            spinGur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    String selectedItemText = (String) parent.getItemAtPosition(position);
                                    Log.d(TAG, selectedItemText);
                                    if (position >= 0) {
                                        spinGur.setError(null);
                                        sGuruID = listId.get(position);
                                    } else {
                                        sGuruID = "";
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListGuruResponse> call, Throwable t) {

                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
            }
        });
    }

    public void getMyClass() {
        restClient.myClass(homeActivity.sToken).enqueue(new Callback<ListClassResponse>() {
            @Override
            public void onResponse(Call<ListClassResponse> call, Response<ListClassResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    if (response.body().isStatus()) {
                        if (response.body().getListClass() != null)
                            if (response.body().getListClass().size() > 0) {
                                listClas = new ArrayList<>();
                                List<ListClas> tempList = response.body().getListClass();

                                for (int i = tempList.size() - 1; i >= 0; i--) {
                                    ListClas lc = tempList.get(i);
                                    if (checkClass(lc.getStartTime(), lc.getFinishTime()).equals("WAIT") ||
                                            (checkClass(lc.getStartTime(), lc.getFinishTime()).equals("PLAY"))) {
                                        listClas.add(lc);
                                    }
                                }

//                                listClas = tempList;
                                kelasAdapter = new KelasAdapter(homeActivity, listClas, new KelasAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(ListClas item) {
                                        Log.i(TAG, "onItemClick: " + item.toString());
                                    }

                                    @Override
                                    public void onItemClick(ListClas item, String w) {
                                        Log.i(TAG, "onItemClick: " + item.toString() + " \n" + w);
                                        if (w.equals("join"))
                                            joinClass(item);
                                        else if (w.equals("enter")) {
                                            Intent in = new Intent(homeActivity, Classroom.class);
                                            in.putExtra("id_user", homeActivity.idUser);
                                            in.putExtra("class_id", item.getClassId());
                                            startActivity(in);
                                        }
                                    }
                                });

                                recyclerView.setAdapter(kelasAdapter);
                                tvNoKelas.setVisibility(View.GONE);
                            }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListClassResponse> call, Throwable t) {
            }
        });
    }

    public void joinClass(ListClas listClas) {
        restClient.joinClass(listClas.getClassId(), homeActivity.idUser, homeActivity.sNama, homeActivity.sEmail, homeActivity.sUserType).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getMyClass();
                    }
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void doBack() {

        if (back == 1) {
            homeActivity.tvTitle.setText("Mengaji");
            radioGroup.clearCheck();
            llAwal.setVisibility(View.VISIBLE);
            llMMandiri.setVisibility(View.GONE);
            spinGur.setSelection(0);
            imageView.setImageResource(0);
            llPlay.setVisibility(View.GONE);
            tvHasil.setText("");
            tvHasil.setVisibility(View.GONE);

            sGuruID = "";
            sHasilScan = null;
            sSound = "";
            b64 = "";

            imgO.setImageDrawable(homeActivity.getDrawable(R.drawable.monlineg));
            imgG.setImageDrawable(homeActivity.getDrawable(R.drawable.mgurug));
            llMengaji.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
            llOnline.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
            back = 0;
        } else if (back == 2) {
            homeActivity.tvTitle.setText("Mengaji");
            radioGroup.clearCheck();
            llAwal.setVisibility(View.VISIBLE);
            llMOnline.setVisibility(View.GONE);

            imgO.setImageDrawable(homeActivity.getDrawable(R.drawable.monlineg));
            imgG.setImageDrawable(homeActivity.getDrawable(R.drawable.mgurug));
            llMengaji.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
            llOnline.setBackground(homeActivity.getDrawable(R.drawable.rounded_blues));
            back = 0;
        } else {
            homeActivity.doMenuFirst();
        }
    }

    public void clearRG() {
        radioGroup.clearCheck();
    }

    @Override
    public void onProgressUpdate(int percentage) {

        tvPre.setText(percentage + "%");
    }

    @Override
    public void onError() {

        tvPre.setText("Uploaded Failed!");
        tvPre.setTextColor(Color.RED);
    }

    @Override
    public void onFinish() {

        tvPre.setText("Uploaded Successfully");
    }

    @Override
    public void uploadStart() {

        tvPre.setText("0%");
//        Toast.makeText(homeActivity, "Upload started", Toast.LENGTH_LONG).show();
    }


    private File createTempFile(Bitmap bitmap) {
        File file = new File(homeActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                , System.currentTimeMillis() + "_image.jpg");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

//        bitmap.compress(Bitmap.CompressFormat.WEBP, 0, bos);
//        byte[] bitmapdata = bos.toByteArray();
//        //write the bytes in file

        //convert the decoded bitmap to stream
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        /*compress bitmap into byteArrayOutputStream
            Bitmap.compress(Format, Quality, OutputStream)
            Where Quality ranges from 1 - 100.
         */
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);

        try {
            FileOutputStream fos = new FileOutputStream(file);
//            fos.write(bitmapdata);
            fos.write(byteArrayOutputStream.toByteArray());
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public void showDialogss(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(homeActivity);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                clearRG();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        back = 0;
                    }
                }, 500);
            }
        });
        builder.show();
    }

    public void showDialog(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(homeActivity);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("Iya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ShowProgressDialog();
                tvPre.setText("Uploading data");

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        sendToServer();
                    }
                }, 500);
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void ShowProgressDialog() {
        dialogBuilder = new AlertDialog.Builder(homeActivity);
        LayoutInflater inflater = (LayoutInflater) homeActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        tvPre = dialogView.findViewById(R.id.textViewDialog);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }
}
