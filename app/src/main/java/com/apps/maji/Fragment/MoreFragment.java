package com.apps.maji.Fragment;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.AboutActivity;
import com.apps.maji.ChangeProfileActivity;
import com.apps.maji.GlideApp;
import com.apps.maji.GuruActivity;
import com.apps.maji.HomeActivity;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataProfile;
import com.apps.maji.Model.DetailTeacher;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.PasswordActivity;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.bumptech.glide.request.RequestOptions;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {

    private static final String TAG = "MoreFragment";
    SessionManager sessionManager;
    View btnLogout, llGuru, llDetGuru, llPass, llGuruM, llAbout, llNilai;
    HomeActivity homeActivity;
    TextView tvnama, tvemail, tvschool, tvTutorName, tvVersion, tvUbah;
    ImageView img, imgTutor;
    DetailTeacher detailTeacher;
    public static final int DIALOG_QUEST_CODE = 300;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_more, container, false);

        homeActivity = (HomeActivity) getActivity();
        sessionManager = new SessionManager(getContext());

        tvUbah = v.findViewById(R.id.tvUbah);
        tvUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, ChangeProfileActivity.class));
            }
        });

        llPass = v.findViewById(R.id.llPass);
        llPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, PasswordActivity.class));
            }
        });
        llGuruM = v.findViewById(R.id.llGuru);
        llGuruM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, GuruActivity.class));
            }
        });
        llAbout = v.findViewById(R.id.llAbout);
        llAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, AboutActivity.class));
            }
        });
        llNilai = v.findViewById(R.id.llNilai);
        llNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = homeActivity.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        btnLogout = v.findViewById(R.id.llLogouts);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: logout");
                showDialog("Apakah Anda ingin keluar dari akun ini?");
            }
        });


        tvnama = v.findViewById(R.id.tvNama);
        tvemail = v.findViewById(R.id.tvEmail);
        tvschool = v.findViewById(R.id.tvSchool);
        tvVersion = v.findViewById(R.id.tvVersion);
        tvVersion.setText("v" + Method.checkVersion(homeActivity));

        imgTutor = v.findViewById(R.id.imageViewC);
        img = v.findViewById(R.id.imgMy);

//        setInfo();
        setTutor();
        return v;
    }

    public void setInfo() {
        String ds = sessionManager.getData();
        Data d = Data.create(ds);
        String dsp = sessionManager.getDataProfile();
        DataProfile dp = DataProfile.create(dsp);
        tvnama.setText(dp.getName());
        tvemail.setText(dp.getEmail());
        tvschool.setText(d.getSchoolName());

        int im;
        if (dp.getJk().equals("L"))
            im = R.drawable.icon_man;
        else
            im = R.drawable.icon_girl;

        GlideApp.with(homeActivity)
                .load(dp.getUserImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(im)
                .into(img);
        Log.i(TAG, "setInfo: " + d.getEmail());
        if (d.getSchoolName() == null || d.getSchoolName().equals("")) {
            updateFCM();
            Toast.makeText(homeActivity, "Session login expired, please relogin", Toast.LENGTH_SHORT).show();
        } else
            tvschool.setText(d.getSchoolName());
    }

    public void setTutor() {

        String dt = sessionManager.getDataMyTutor();
        if (dt != null) {
            detailTeacher = DetailTeacher.create(dt);
            if (tvTutorName != null)
                tvTutorName.setText(detailTeacher.getName());
            if (imgTutor != null)
                GlideApp.with(homeActivity)
                        .load(detailTeacher.getUserImage())
                        .apply(RequestOptions.circleCropTransform())
                        .into(imgTutor);
        }
    }

    public void updateFCM() {
        homeActivity.restApi.sendFCM(homeActivity.idUser, "kosong", sessionManager.getToken()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }

                sessionManager.logoutUser();
                homeActivity.finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(homeActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_teacher);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.title)).setText(detailTeacher.getName());
//        ((TextView) dialog.findViewById(R.id.content)).setText(detailTeacher.getName());
        llGuru = dialog.findViewById(R.id.llGuru);
        llGuru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.getGuru();
                dialog.dismiss();
            }
        });
        GlideApp.with(homeActivity)
                .load(detailTeacher.getUserImage())
                .apply(RequestOptions.circleCropTransform())
                .into(((CircleImageView) dialog.findViewById(R.id.image)));

        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public void showDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateFCM();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void doBack() {
        homeActivity.doMenuFirst();
    }
}
