package com.apps.maji.Fragment;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.maji.Adapter.ChatAdapter;
import com.apps.maji.HomeActivity;
import com.apps.maji.Model.ChatReceiveMsg;
import com.apps.maji.Model.ChatSendMsgResp;
import com.apps.maji.Model.ChatUnReadResponse;
import com.apps.maji.Model.MessagesChat;
import com.apps.maji.R;
import com.apps.maji.Utils.Constant;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {


    private static final String TAG = "ChatsFragment";
    View llChats, llAwal, llChat;
    ChatAdapter chatAdapter;
    RecyclerView mRecyclerView;
    EditText etChat;
    View v;
    List<MessagesChat> chatPojos;
    HomeActivity homeActivity;
    int back = 0;
    View fabSend;
    ImageView imgBack;
    TextView tvTitle, tvMsgLast;

    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_chats, container, false);
        homeActivity = (HomeActivity) getActivity();
        Button btnSend = v.findViewById(R.id.button_send_message);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickSend();
            }
        });

        tvTitle = v.findViewById(R.id.tvTitle);
        tvMsgLast = v.findViewById(R.id.tvMsgLast);

        fabSend = v.findViewById(R.id.fabSend);
        fabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickSend();
            }
        });

        etChat = v.findViewById(R.id.edittext_chat_message);
        etChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToBottom();
                    }
                }, 200);
                scrollToBottom();
            }
        });

        chatPojos = new ArrayList<>();

        llAwal = v.findViewById(R.id.llAwal);
        llChat = v.findViewById(R.id.llChat);
        llChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAwal.setVisibility(View.GONE);
                llChats.setVisibility(View.VISIBLE);
                back = 1;
                tvTitle.setText("Chat Grup");
                onRead();
                onReceive();
            }
        });
        llChats = v.findViewById(R.id.llChats);

        imgBack = v.findViewById(R.id.imgMsg);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doBack();
            }
        });

        mRecyclerView = v.findViewById(R.id.recycler_viewChat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        chatAdapter = new ChatAdapter(getContext(), chatPojos);
        mRecyclerView.setAdapter(chatAdapter);
        scrollToBottom();
        return v;
    }

    public void doBack() {
        if (back == 1) {

            llAwal.setVisibility(View.VISIBLE);
            llChats.setVisibility(View.GONE);
            back = 0;
            tvTitle.setText("Chat");
        } else {
            homeActivity.doHideChat();
        }
    }

    public void ClickSend() {
        String message = etChat.getText().toString();
        if (!message.trim().equals("")) {
            sumbitMsg(message);
        }
        etChat.setText("");
    }

    public void sumbitMsg(final String msg) {
        homeActivity.restApiChat.chatSendMsg(homeActivity.detailTeacher.getId(), homeActivity.myData.getId(), msg).enqueue(new Callback<ChatSendMsgResp>() {
            @Override
            public void onResponse(Call<ChatSendMsgResp> call, Response<ChatSendMsgResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {
                            homeActivity.mSocket.emit(Constant.CHAT_MESSAGE, msg);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatSendMsgResp> call, Throwable t) {

            }
        });
    }

    public void onReceive() {
        homeActivity.restApiChat.chatReceiveMsg(homeActivity.detailTeacher.getId()).enqueue(new Callback<ChatReceiveMsg>() {
            @Override
            public void onResponse(Call<ChatReceiveMsg> call, Response<ChatReceiveMsg> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        chatPojos.clear();
                        chatPojos.addAll(response.body().getMessagesChat());

                        chatAdapter.notifyDataSetChanged();
                        scrollToBottom();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatReceiveMsg> call, Throwable t) {
                Log.e(TAG, "onFailure: onReceive");
                t.printStackTrace();
            }
        });
    }

    public void onUnread() {
        homeActivity.restApiChat.chatUnreadMsg(homeActivity.myData.getId()).enqueue(new Callback<ChatUnReadResponse>() {
            @Override
            public void onResponse(Call<ChatUnReadResponse> call, Response<ChatUnReadResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getMessagesUnread().size() > 0) {
                            for (int i = 0; i < response.body().getMessagesUnread().size(); i++) {
                                if (response.body().getMessagesUnread().get(i).getNameGroup().equals(homeActivity.detailTeacher.getName())) {
                                    tvMsgLast.setText(response.body().getMessagesUnread().get(i).getLastMessage());
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatUnReadResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: onReceive");
                t.printStackTrace();

            }
        });
    }

    public void onRead() {
        homeActivity.restApiChat.chatReadMsg(homeActivity.myData.getId(), homeActivity.detailTeacher.getId()).enqueue(new Callback<ChatSendMsgResp>() {
            @Override
            public void onResponse(Call<ChatSendMsgResp> call, Response<ChatSendMsgResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: "+response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatSendMsgResp> call, Throwable t) {
                Log.e(TAG, "onFailure: onReceive");
                t.printStackTrace();

            }
        });
    }

    private void scrollToBottom() {
        mRecyclerView.scrollToPosition(chatPojos.size() - 1);
        if (chatPojos.size() > 0)
            tvMsgLast.setText(chatPojos.get(chatPojos.size()-1).getMessage());
    }
}
