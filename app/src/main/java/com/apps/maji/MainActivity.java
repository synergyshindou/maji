package com.apps.maji;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.Interface.OnSpinerItemClick;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.GetKecamatanResponse;
import com.apps.maji.Model.GetKotaResponse;
import com.apps.maji.Model.GetProvinsiResponse;
import com.apps.maji.Model.GetSekolahResponse;
import com.apps.maji.Model.LoginResponse;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.apps.maji.Widget.SpinnerDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.livinglifetechway.quickpermissions.annotations.WithPermissions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.Constant.QRID;
import static com.apps.maji.Utils.Constant.QRREGISTER;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    RadioGroup radioGroup;
    RadioButton radioSexButton;
    Spinner spinSchool;
    TextInputEditText etName, etEmail, etSandi, etSandiKonfirm, etPhone, etEmailF;
    TextInputEditText etEmailLogin, etPassLogin, etSNe, etSAe, etJenjang, etProvinsie, etKotae, etKece;
    View llDaftar, llLogin, llForgot, mProgressView, llQRCode;

    ApiConfig mAPIService;

    String[] sSchoolAddress, sSchoolName;
    String rgender = "", sIdPType = "", sIdPPro = "", sIdPKab = "", sIdPKec = "", sSchoolId = "",
            QRCode = "";

    SessionManager sessionManager;
    ArrayList<String> items = new ArrayList<>();
    ArrayList<String> itemsclick = new ArrayList<>();
    SpinnerDialog spinnerDialog;

    CheckBox cbQRNo;

    TextView tvScanned;

    int back = 0;

    TextView tvVersion;

    ImageView imgHead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(this);
        if (sessionManager.isLoggedIn()) {

            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
            return;
        }
        InitView();
        mAPIService = AppConfig.getRetrofit().create(ApiConfig.class);

        etJenjang = findViewById(R.id.etJenjang);
        etProvinsie = findViewById(R.id.etProvinsie);
        etKotae = findViewById(R.id.etKotae);
        etKece = findViewById(R.id.etKece);
        etSNe = findViewById(R.id.etSNe);
        etSAe = findViewById(R.id.etSAe);

        methodWithPermissions();

        cbQRNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    llQRCode.setVisibility(View.GONE);
                else llQRCode.setVisibility(View.VISIBLE);
            }
        });
        Uri data = getIntent().getData();
        if (data != null){
            Log.i(TAG, "onCreate: "+data.toString());

            List<String> params = data.getPathSegments();
            if (params.size() > 0)
                Toast.makeText(this, params.get(0), Toast.LENGTH_LONG).show();
        }

        initFCM();
    }

    @Override
    public void onBackPressed() {
        if (back == 1) {
            llDaftar.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            back = 0;
        } else if (back == 2) {
            llForgot.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            back = 0;
        } else
            super.onBackPressed();
    }

    void initFCM(){
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                    }
                });
        // [END retrieve_current_token]
    }

    public void InitView() {
        llDaftar = findViewById(R.id.llDaftar);
        llLogin = findViewById(R.id.llLogin);
        llForgot = findViewById(R.id.llForgot);
        mProgressView = findViewById(R.id.login_progress);

        etEmailLogin = findViewById(R.id.etEmailLogin);
        etPassLogin = findViewById(R.id.etPass);

        etName = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmail);
        etEmailF = findViewById(R.id.etEmailForgot);
        etSandi = findViewById(R.id.etSandi);
        etSandiKonfirm = findViewById(R.id.etSandiKonfirm);
        etPhone = findViewById(R.id.etTelepon);
        radioGroup = findViewById(R.id.radioGroup1);
        spinSchool = findViewById(R.id.spinSchool);

        tvScanned = findViewById(R.id.tvScanned);

        cbQRNo = findViewById(R.id.cbBuku);

        llQRCode = findViewById(R.id.llQRCode);

        tvVersion = findViewById(R.id.tvVersion);
        tvVersion.setText("Ver. "+ Method.checkVersion(this));

        imgHead = findViewById(R.id.imgHeads);

        GlideApp.with(this)
                .load(R.drawable.mosque_blank)
                .into(imgHead);

    }

    public void sendData(View v) {
        int selectedId = radioGroup.getCheckedRadioButtonId();
        if (etName.getText().toString().length() == 0) {
            etName.setError("Tidak Boleh Kosong");
            etName.requestFocus();
        } else if (etEmail.getText().toString().length() == 0){
            etEmail.setError("Tidak Boleh Kosong");
            etEmail.requestFocus();
        } else if (etSandi.getText().toString().length() == 0){
            etSandi.setError("Tidak Boleh Kosong");
            etSandi.requestFocus();
        } else if(!etSandi.getText().toString().equals(etSandiKonfirm.getText().toString())){
            etSandiKonfirm.setError("Password Tidak Sama");
            etSandiKonfirm.requestFocus();
        } else if (etPhone.getText().toString().length() == 0){
            etPhone.setError("Tidak Boleh Kosong");
            etPhone.requestFocus();
        } else if (sIdPType.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Jenjang Dahulu", Toast.LENGTH_SHORT).show();
        } else if (sIdPPro.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Provinsi Dahulu", Toast.LENGTH_SHORT).show();
        } else if (sIdPKab.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Kota Dahulu", Toast.LENGTH_SHORT).show();
        } else if (sIdPKec.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Kecamatan Dahulu", Toast.LENGTH_SHORT).show();
        } else {
            radioSexButton = findViewById(selectedId);
            if (radioSexButton == null) {
                Toast.makeText(this, "Mohon Pilih Jenis Kelamin Dahulu", Toast.LENGTH_SHORT).show();
            } else {
                if (radioSexButton.getText().equals("Pria"))
                    rgender = "L";
                else
                    rgender = "P";
                Log.i(TAG, "sendData: " + etSandiKonfirm.getText().toString().trim());
                showProgressD(true);
                mAPIService.register(etEmail.getText().toString(),
                        etSandiKonfirm.getText().toString().trim(),
                        etName.getText().toString(),
                        rgender,
                        etPhone.getText().toString(),
                        "student",
                        sSchoolId, QRCode)
                        .enqueue(new Callback<ServerResponse>() {
                            @Override
                            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                                Log.i(TAG, "onResponse: M " + response.message());
                                if (response.body() != null) {
                                    Log.i(TAG, "onResponse: B " + response.body());

                                    if (response.body().isSuccess()) {
                                        showProgress(false);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setTitle(getString(R.string.app_name));
                                        builder.setMessage(response.body().getMessage());
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                            }
                                        });
                                        builder.show();
                                    } else {
                                        showProgressD(false);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setTitle(getString(R.string.app_name));
                                        builder.setMessage(response.body().getMessage());
                                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                            }
                                        });
                                        builder.show();
                                    }
                                } else {
                                    showDialog("Please and try again. . .");
                                    showProgressD(false);
                                }

                            }

                            @Override
                            public void onFailure(Call<ServerResponse> call, Throwable t) {
                                Log.e(TAG, "onFailure: " + t.getMessage());
                                showProgressD(false);
                                showDialog("Check connection and try again. . .");
                            }
                        });
            }
        }
    }

    public void doLogin() {
        if (etEmailLogin.getText().toString().length() == 0) {
            etEmailLogin.setError("Tidak Boleh Kosong");
            etEmailLogin.requestFocus();
        } else if (etPassLogin.getText().toString().length() == 0) {
            etPassLogin.setError("Tidak Boleh Kosong");
            etPassLogin.requestFocus();
        } else {
            showProgress(true);
            mAPIService.login(etEmailLogin.getText().toString().trim(), etPassLogin.getText().toString().trim()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                Data data = response.body().getData();
                                if (data.getUsertype().equals("student")) {
                                    String serialize = data.serialize();
                                    Log.i(TAG, "onResponse: Data SERVER " + data.getEmail());
                                    sessionManager.createData(serialize);
                                    sessionManager.createToken(response.body().getAccessToken());
                                    saveFCM(data.getId());
                                } else {
                                    showDialog("Email Anda sudah terdaftar sebagai akun Guru.");
                                    showProgress(false);
                                }
                            } else {
                                showDialog(response.body().getMessage());
                                showProgress(false);
                            }
                        } else {
                            if (response.body().getCode() == 403)
                                showDialogSendMail(response.body().getMessage());
                            else
                                showDialog(response.body().getMessage());
                            showProgress(false);
                        }
                    } else {
                        showDialog("Please try again. . .");
                        showProgress(false);
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    showProgress(false);
                    showDialog("Check connection and try again. . .");

                }
            });
        }
    }

    public void sendForgot(View v) {
        if (etEmailF.getText().toString().length() == 0) {
            etEmailF.setError("Tidak Boleh Kosong");
            etEmailF.requestFocus();
        } else {
            showProgressF(true);
            Log.i(TAG, "sendForgot: To server");
            mAPIService.forgotPassword(etEmailF.getText().toString().trim()).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    showProgressF(false);
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isSuccess()) {
                            showDialog(response.body().getMessage());
                            llForgot.setVisibility(View.GONE);
                            llLogin.setVisibility(View.VISIBLE);
                            back = 0;
                        } else {
                            showDialog(response.body().getMessage());
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Check your connection and try again. . .", Toast.LENGTH_SHORT).show();
                    showProgressF(false);
                }
            });
        }
    }

    void resendEmail(){
        mAPIService.resendActivation(etEmailLogin.getText().toString().trim()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                showProgress(false);
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                    if (response.body().isSuccess()) {
                        showDialog(response.body().getMessage());
                    } else {
                        showDialog(response.body().getMessage());
                    }
                } else {
                    showDialog("please try again. . .");
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                showDialog("Check connection and try again. . .");
                showProgress(false);
            }
        });
    }

    void saveFCM(String iduser){
        mAPIService.sendFCM(iduser, sessionManager.getTokenFCM(), sessionManager.getToken()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {

                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }
                sessionManager.createTokenFCM();
                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                showProgress(false);
                showDialog("Check connection and try again. . .");
            }
        });
    }

    public void onForgot(View v) {
        llForgot.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        back = 2;
    }

    public void onDaftar(View v) {
        llDaftar.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        back = 1;
    }

    public void onLogin(View v) {
        doLogin();
    }

    public void onScanQR(View v) {
        startActivityForResult(new Intent(MainActivity.this, QRScanActivity.class), QRREGISTER);
    }

    public void showJenjang(View v) {
        items = new ArrayList<>();
        items.add("SD / MI");
        items.add("SMP / MTs");
        items.add("SMA / MA");
        items.add("SMK / MAK");
        itemsclick = new ArrayList<>();
        itemsclick.add("SD");
        itemsclick.add("SMP");
        itemsclick.add("SMA");
        itemsclick.add("SMK");

        spinnerDialog = new SpinnerDialog(MainActivity.this, items);
        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                etJenjang.setText(item);
                etJenjang.setError(null);
                etProvinsie.setText("");
                etKotae.setText("");
                etKece.setText("");
                etSNe.setText("");
                etSAe.setText("");
                sIdPType = itemsclick.get(position);
                sIdPPro = "";
                sIdPKab = "";
                sIdPKec = "";
                sSchoolId = "";
            }

            @Override
            public void onTextClick(View v) {

            }
        });
        spinnerDialog.showSpinerDialog();
    }

    public void showProvinsi(View v) {
        if (sIdPType.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Jenjang Dahulu", Toast.LENGTH_SHORT).show();
        } else
            getProvinsi("provinsi");
    }

    public void showKota(View v) {

        if (sIdPPro.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Provinsi Dahulu", Toast.LENGTH_SHORT).show();
        } else {
            getProvinsi("kota");
        }
    }

    public void showKec(View v) {
        if (sIdPKab.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Kota Dahulu", Toast.LENGTH_SHORT).show();
        } else {
            getProvinsi("kec");
        }
    }

    public void showSchools(View v) {
        if (sIdPKec.length() == 0) {
            Toast.makeText(this, "Mohon Pilih Kecamatan Dahulu", Toast.LENGTH_SHORT).show();
        } else {
            getProvinsi("school");
        }
    }

    void getProvinsi(String sWhat) {
        if (sWhat.equals("provinsi")) {
            mAPIService.getProvinsi(sIdPType).enqueue(new Callback<GetProvinsiResponse>() {
                @Override
                public void onResponse(Call<GetProvinsiResponse> call, Response<GetProvinsiResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            items = new ArrayList<>();
                            if (response.body().getDataProvinsi().size() > 0) {
                                for (int i = 0; i < response.body().getDataProvinsi().size(); i++) {
                                    items.add(response.body().getDataProvinsi().get(i).getProvinsi());
                                }

                                spinnerDialog = new SpinnerDialog(MainActivity.this, items);// With 	Animation
                                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        etProvinsie.setText(item);
                                        etProvinsie.setError(null);
                                        etKotae.setText("");
                                        etKece.setText("");
                                        etSNe.setText("");
                                        etSAe.setText("");
                                        sIdPPro = item;
                                        sIdPKab = "";
                                        sIdPKec = "";
                                        sSchoolId = "";
                                    }

                                    @Override
                                    public void onTextClick(View v) {

                                    }
                                });
                                spinnerDialog.showSpinerDialog();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetProvinsiResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (sWhat.equals("kota")) {
            mAPIService.getKota(sIdPType, sIdPPro).enqueue(new Callback<GetKotaResponse>() {
                @Override
                public void onResponse(Call<GetKotaResponse> call, Response<GetKotaResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            items = new ArrayList<>();
                            if (response.body().getDataKota().size() > 0) {
                                for (int i = 0; i < response.body().getDataKota().size(); i++) {
                                    items.add(response.body().getDataKota().get(i).getKabupaten());
                                }

                                spinnerDialog = new SpinnerDialog(MainActivity.this, items);// With 	Animation
                                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        etKotae.setText(item);
                                        etKotae.setError(null);
                                        etKece.setText("");
                                        etSNe.setText("");
                                        etSAe.setText("");
                                        sIdPKab = item;
                                        sIdPKec = "";
                                        sSchoolId = "";
                                    }

                                    @Override
                                    public void onTextClick(View v) {

                                    }
                                });
                                spinnerDialog.showSpinerDialog();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetKotaResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (sWhat.equals("kec")) {
            mAPIService.getKec(sIdPType, sIdPPro, sIdPKab).enqueue(new Callback<GetKecamatanResponse>() {
                @Override
                public void onResponse(Call<GetKecamatanResponse> call, Response<GetKecamatanResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            items = new ArrayList<>();
                            if (response.body().getDataKecamatan().size() > 0) {
                                for (int i = 0; i < response.body().getDataKecamatan().size(); i++) {
                                    items.add(response.body().getDataKecamatan().get(i).getKecamatan());
                                }

                                spinnerDialog = new SpinnerDialog(MainActivity.this, items);// With 	Animation
                                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        etKece.setText(item);
                                        etKece.setError(null);
                                        etSNe.setText("");
                                        etSAe.setText("");
                                        sIdPKec = item;
                                        sSchoolId = "";
                                    }

                                    @Override
                                    public void onTextClick(View v) {

                                    }
                                });
                                spinnerDialog.showSpinerDialog();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetKecamatanResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (sWhat.equals("school")) {
            mAPIService.getSekolah(sIdPType, sIdPPro, sIdPKab, sIdPKec).enqueue(new Callback<GetSekolahResponse>() {
                @Override
                public void onResponse(Call<GetSekolahResponse> call, Response<GetSekolahResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            items = new ArrayList<>();
                            if (response.body().getDataSchool().size() > 0) {
                                sSchoolAddress = new String[response.body().getDataSchool().size()];
                                sSchoolName = new String[response.body().getDataSchool().size()];
                                for (int i = 0; i < response.body().getDataSchool().size(); i++) {
                                    items.add(response.body().getDataSchool().get(i).getSchoolName());
                                    sSchoolAddress[i] = response.body().getDataSchool().get(i).getSchoolAddress();
                                    sSchoolName[i] = response.body().getDataSchool().get(i).getSchoolId();
                                }

                                spinnerDialog = new SpinnerDialog(MainActivity.this, items);// With 	Animation
                                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        etSNe.setError(null);
                                        etSNe.setText(item);
                                        etSAe.setText(sSchoolAddress[position]);
                                        sSchoolId = sSchoolName[position];
                                    }

                                    @Override
                                    public void onTextClick(View v) {

                                    }
                                });
                                spinnerDialog.showSpinerDialog();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetSekolahResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QRREGISTER) {
            if (resultCode == RESULT_OK) {
//                Toast.makeText(this, data.getStringExtra(QRID), Toast.LENGTH_SHORT).show();
                Log.i(TAG, "onActivityResult: " + data.getStringExtra(QRID));
                tvScanned.setVisibility(View.VISIBLE);
                QRCode = data.getStringExtra(QRID);
            }
        }
    }

    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llLogin.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showProgressF(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llForgot.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showProgressD(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llDaftar.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @WithPermissions(
            permissions = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
    )
    public void methodWithPermissions() {

    }

    public void showDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogSendMail(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setNegativeButton("Resend Email", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgress(true);
                resendEmail();
            }
        });
        builder.show();
    }
}
