
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessagesChat implements Parcelable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_user_group")
    @Expose
    private int idUserGroup;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("is_read")
    @Expose
    private String isRead;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user_group")
    @Expose
    private UserGroup userGroup;
    public final static Parcelable.Creator<MessagesChat> CREATOR = new Creator<MessagesChat>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MessagesChat createFromParcel(Parcel in) {
            MessagesChat instance = new MessagesChat();
            instance.id = ((int) in.readValue((int.class.getClassLoader())));
            instance.idUserGroup = ((int) in.readValue((int.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.isRead = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.userGroup = ((UserGroup) in.readValue((UserGroup.class.getClassLoader())));
            return instance;
        }

        public MessagesChat[] newArray(int size) {
            return (new MessagesChat[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idUserGroup
     */
    public int getIdUserGroup() {
        return idUserGroup;
    }

    /**
     * 
     * @param idUserGroup
     *     The id_user_group
     */
    public void setIdUserGroup(int idUserGroup) {
        this.idUserGroup = idUserGroup;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The isRead
     */
    public String getIsRead() {
        return isRead;
    }

    /**
     * 
     * @param isRead
     *     The is_read
     */
    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    /**
     * 
     * @return
     *     The userGroup
     */
    public UserGroup getUserGroup() {
        return userGroup;
    }

    /**
     * 
     * @param userGroup
     *     The user_group
     */
    public void setUserGroup(UserGroup userGroup) {
        this.userGroup = userGroup;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(idUserGroup);
        dest.writeValue(message);
        dest.writeValue(isRead);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(userGroup);
    }

    public int describeContents() {
        return  0;
    }

}
