
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProvinsiResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private List<DataProvinsi> dataProvinsi = new ArrayList<DataProvinsi>();
    public final static Parcelable.Creator<GetProvinsiResponse> CREATOR = new Creator<GetProvinsiResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GetProvinsiResponse createFromParcel(Parcel in) {
            GetProvinsiResponse instance = new GetProvinsiResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.dataProvinsi, (com.apps.maji.Model.DataProvinsi.class.getClassLoader()));
            return instance;
        }

        public GetProvinsiResponse[] newArray(int size) {
            return (new GetProvinsiResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The dataProvinsi
     */
    public List<DataProvinsi> getDataProvinsi() {
        return dataProvinsi;
    }

    /**
     * 
     * @param dataProvinsi
     *     The dataProvinsi
     */
    public void setDataProvinsi(List<DataProvinsi> dataProvinsi) {
        this.dataProvinsi = dataProvinsi;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(code);
        dest.writeList(dataProvinsi);
    }

    public int describeContents() {
        return  0;
    }

}
