package com.apps.maji.Model;

/**
 * Created by delaroystudios on 10/5/2016.
 */

import com.google.gson.annotations.SerializedName;


public class ServerResponse {

    // variable name should be same as in the json response from php
    @SerializedName("status")
    boolean success;

    @SerializedName("code")
    int code;

    @SerializedName("message")
    String message;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ServerResponse{");
        sb.append("success=").append(success);
        sb.append(", code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}