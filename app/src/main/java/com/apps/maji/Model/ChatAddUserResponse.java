
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatAddUserResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataAddUser dataAddUser;
    public final static Parcelable.Creator<ChatAddUserResponse> CREATOR = new Creator<ChatAddUserResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ChatAddUserResponse createFromParcel(Parcel in) {
            ChatAddUserResponse instance = new ChatAddUserResponse();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.dataAddUser = ((DataAddUser) in.readValue((DataAddUser.class.getClassLoader())));
            return instance;
        }

        public ChatAddUserResponse[] newArray(int size) {
            return (new ChatAddUserResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataAddUser
     */
    public DataAddUser getDataAddUser() {
        return dataAddUser;
    }

    /**
     * 
     * @param dataAddUser
     *     The dataAddUser
     */
    public void setDataAddUser(DataAddUser dataAddUser) {
        this.dataAddUser = dataAddUser;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(dataAddUser);
    }

    public int describeContents() {
        return  0;
    }

}
