
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatGroupCreateResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataCreateGroup dataCreateGroup;
    public final static Parcelable.Creator<ChatGroupCreateResponse> CREATOR = new Creator<ChatGroupCreateResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ChatGroupCreateResponse createFromParcel(Parcel in) {
            ChatGroupCreateResponse instance = new ChatGroupCreateResponse();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.dataCreateGroup = ((DataCreateGroup) in.readValue((DataCreateGroup.class.getClassLoader())));
            return instance;
        }

        public ChatGroupCreateResponse[] newArray(int size) {
            return (new ChatGroupCreateResponse[size]);
        }

    };

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The dataCreateGroup
     */
    public DataCreateGroup getDataCreateGroup() {
        return dataCreateGroup;
    }

    /**
     * @param dataCreateGroup The dataCreateGroup
     */
    public void setDataCreateGroup(DataCreateGroup dataCreateGroup) {
        this.dataCreateGroup = dataCreateGroup;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(dataCreateGroup);
    }

    public int describeContents() {
        return 0;
    }

}
