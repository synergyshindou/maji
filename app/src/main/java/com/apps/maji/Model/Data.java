package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("jk")
    @Expose
    private String jk;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            Data instance = new Data();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.email = ((String) in.readValue((String.class.getClassLoader())));
            instance.noHp = ((String) in.readValue((String.class.getClassLoader())));
            instance.jk = ((String) in.readValue((String.class.getClassLoader())));
            instance.usertype = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolName = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    }
            ;

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The noHp
     */
    public String getNoHp() {
        return noHp;
    }

    /**
     *
     * @param noHp
     *     The no_hp
     */
    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    /**
     *
     * @return
     *     The jk
     */
    public String getJk() {
        return jk;
    }

    /**
     *
     * @param jk
     *     The jk
     */
    public void setJk(String jk) {
        this.jk = jk;
    }

    /**
     *
     * @return
     *     The usertype
     */
    public String getUsertype() {
        return usertype;
    }

    /**
     *
     * @param usertype
     *     The usertype
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     *
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Data{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", noHp='").append(noHp).append('\'');
        sb.append(", jk='").append(jk).append('\'');
        sb.append(", usertype='").append(usertype).append('\'');
        sb.append(", schoolName='").append(schoolName).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(noHp);
        dest.writeValue(jk);
        dest.writeValue(usertype);
        dest.writeValue(schoolName);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return  0;
    }

    public String serialize() {
        // Serialize this class into a JSON string using GSON
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    static public Data create(String serializedData) {
        // Use GSON to instantiate this class using the JSON representation of the state
        Gson gson = new Gson();
        return gson.fromJson(serializedData, Data.class);
    }
}
