
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSchool implements Parcelable
{

    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("school_address")
    @Expose
    private String schoolAddress;
    @SerializedName("provinsi")
    @Expose
    private String provinsi;
    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("kelurahan")
    @Expose
    private String kelurahan;
    @SerializedName("npsn")
    @Expose
    private String npsn;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("school_callnum")
    @Expose
    private Object schoolCallnum;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    public final static Parcelable.Creator<DataSchool> CREATOR = new Creator<DataSchool>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataSchool createFromParcel(Parcel in) {
            DataSchool instance = new DataSchool();
            instance.schoolId = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolName = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolAddress = ((String) in.readValue((String.class.getClassLoader())));
            instance.provinsi = ((String) in.readValue((String.class.getClassLoader())));
            instance.kabupaten = ((String) in.readValue((String.class.getClassLoader())));
            instance.kecamatan = ((String) in.readValue((String.class.getClassLoader())));
            instance.kelurahan = ((String) in.readValue((String.class.getClassLoader())));
            instance.npsn = ((String) in.readValue((String.class.getClassLoader())));
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.type = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolCallnum = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.description = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.datetime = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataSchool[] newArray(int size) {
            return (new DataSchool[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The schoolId
     */
    public String getSchoolId() {
        return schoolId;
    }

    /**
     * 
     * @param schoolId
     *     The school_id
     */
    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    /**
     * 
     * @return
     *     The schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * 
     * @param schoolName
     *     The school_name
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     * 
     * @return
     *     The schoolAddress
     */
    public String getSchoolAddress() {
        return schoolAddress;
    }

    /**
     * 
     * @param schoolAddress
     *     The school_address
     */
    public void setSchoolAddress(String schoolAddress) {
        this.schoolAddress = schoolAddress;
    }

    /**
     * 
     * @return
     *     The provinsi
     */
    public String getProvinsi() {
        return provinsi;
    }

    /**
     * 
     * @param provinsi
     *     The provinsi
     */
    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    /**
     * 
     * @return
     *     The kabupaten
     */
    public String getKabupaten() {
        return kabupaten;
    }

    /**
     * 
     * @param kabupaten
     *     The kabupaten
     */
    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    /**
     * 
     * @return
     *     The kecamatan
     */
    public String getKecamatan() {
        return kecamatan;
    }

    /**
     * 
     * @param kecamatan
     *     The kecamatan
     */
    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    /**
     * 
     * @return
     *     The kelurahan
     */
    public String getKelurahan() {
        return kelurahan;
    }

    /**
     * 
     * @param kelurahan
     *     The kelurahan
     */
    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    /**
     * 
     * @return
     *     The npsn
     */
    public String getNpsn() {
        return npsn;
    }

    /**
     * 
     * @param npsn
     *     The npsn
     */
    public void setNpsn(String npsn) {
        this.npsn = npsn;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The schoolCallnum
     */
    public Object getSchoolCallnum() {
        return schoolCallnum;
    }

    /**
     * 
     * @param schoolCallnum
     *     The school_callnum
     */
    public void setSchoolCallnum(Object schoolCallnum) {
        this.schoolCallnum = schoolCallnum;
    }

    /**
     * 
     * @return
     *     The description
     */
    public Object getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * 
     * @param datetime
     *     The datetime
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(schoolId);
        dest.writeValue(schoolName);
        dest.writeValue(schoolAddress);
        dest.writeValue(provinsi);
        dest.writeValue(kabupaten);
        dest.writeValue(kecamatan);
        dest.writeValue(kelurahan);
        dest.writeValue(npsn);
        dest.writeValue(status);
        dest.writeValue(type);
        dest.writeValue(schoolCallnum);
        dest.writeValue(description);
        dest.writeValue(datetime);
    }

    public int describeContents() {
        return  0;
    }

}
