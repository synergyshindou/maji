package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataVersion implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("force")
    @Expose
    private String force;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    public final static Parcelable.Creator<DataVersion> CREATOR = new Creator<DataVersion>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataVersion createFromParcel(Parcel in) {
            DataVersion instance = new DataVersion();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.version = ((String) in.readValue((String.class.getClassLoader())));
            instance.force = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.datetime = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataVersion[] newArray(int size) {
            return (new DataVersion[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The version
     */
    public String getVersion() {
        return version;
    }

    /**
     * 
     * @param version
     *     The version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * 
     * @return
     *     The force
     */
    public String getForce() {
        return force;
    }

    /**
     * 
     * @param force
     *     The force
     */
    public void setForce(String force) {
        this.force = force;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * 
     * @param datetime
     *     The datetime
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(version);
        dest.writeValue(force);
        dest.writeValue(name);
        dest.writeValue(datetime);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "DataVersion{" +
                "id='" + id + '\'' +
                ", version='" + version + '\'' +
                ", force='" + force + '\'' +
                ", name='" + name + '\'' +
                ", datetime='" + datetime + '\'' +
                '}';
    }
}
