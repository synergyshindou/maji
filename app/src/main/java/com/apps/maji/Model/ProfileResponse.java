
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataProfile dataProfile;

    @Override
    public String toString() {
        return "ProfileResponse{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", dataProfile=" + dataProfile +
                '}';
    }

    public final static Parcelable.Creator<ProfileResponse> CREATOR = new Creator<ProfileResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProfileResponse createFromParcel(Parcel in) {
            ProfileResponse instance = new ProfileResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.dataProfile = ((DataProfile) in.readValue((DataProfile.class.getClassLoader())));
            return instance;
        }

        public ProfileResponse[] newArray(int size) {
            return (new ProfileResponse[size]);
        }

    };

    /**
     * @return The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The dataProfile
     */
    public DataProfile getDataProfile() {
        return dataProfile;
    }

    /**
     * @param dataProfile The dataProfile
     */
    public void setDataProfile(DataProfile dataProfile) {
        this.dataProfile = dataProfile;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(dataProfile);
    }

    public int describeContents() {
        return 0;
    }

}
