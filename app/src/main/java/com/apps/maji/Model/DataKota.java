
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataKota implements Parcelable
{

    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    public final static Parcelable.Creator<DataKota> CREATOR = new Creator<DataKota>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataKota createFromParcel(Parcel in) {
            DataKota instance = new DataKota();
            instance.kabupaten = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataKota[] newArray(int size) {
            return (new DataKota[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The kabupaten
     */
    public String getKabupaten() {
        return kabupaten;
    }

    /**
     * 
     * @param kabupaten
     *     The kabupaten
     */
    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(kabupaten);
    }

    public int describeContents() {
        return  0;
    }

}
