
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataProvinsi implements Parcelable
{

    @SerializedName("provinsi")
    @Expose
    private String provinsi;
    public final static Parcelable.Creator<DataProvinsi> CREATOR = new Creator<DataProvinsi>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataProvinsi createFromParcel(Parcel in) {
            DataProvinsi instance = new DataProvinsi();
            instance.provinsi = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataProvinsi[] newArray(int size) {
            return (new DataProvinsi[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The provinsi
     */
    public String getProvinsi() {
        return provinsi;
    }

    /**
     * 
     * @param provinsi
     *     The provinsi
     */
    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(provinsi);
    }

    public int describeContents() {
        return  0;
    }

}
