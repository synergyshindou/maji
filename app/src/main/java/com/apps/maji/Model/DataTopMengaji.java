
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataTopMengaji implements Parcelable
{

    @SerializedName("id_mengaji")
    @Expose
    private String idMengaji;
    @SerializedName("id_tutor")
    @Expose
    private String idTutor;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("jk")
    @Expose
    private String jk;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("halaman")
    @Expose
    private String halaman;
    @SerializedName("surat")
    @Expose
    private String surat;
    @SerializedName("ayat")
    @Expose
    private String ayat;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    public final static Parcelable.Creator<DataTopMengaji> CREATOR = new Creator<DataTopMengaji>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataTopMengaji createFromParcel(Parcel in) {
            DataTopMengaji instance = new DataTopMengaji();
            instance.idMengaji = ((String) in.readValue((String.class.getClassLoader())));
            instance.idTutor = ((String) in.readValue((String.class.getClassLoader())));
            instance.idUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.jk = ((String) in.readValue((String.class.getClassLoader())));
            instance.userImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolName = ((String) in.readValue((String.class.getClassLoader())));
            instance.halaman = ((String) in.readValue((String.class.getClassLoader())));
            instance.surat = ((String) in.readValue((String.class.getClassLoader())));
            instance.ayat = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataTopMengaji[] newArray(int size) {
            return (new DataTopMengaji[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The idMengaji
     */
    public String getIdMengaji() {
        return idMengaji;
    }

    /**
     * 
     * @param idMengaji
     *     The id_mengaji
     */
    public void setIdMengaji(String idMengaji) {
        this.idMengaji = idMengaji;
    }

    /**
     * 
     * @return
     *     The idTutor
     */
    public String getIdTutor() {
        return idTutor;
    }

    /**
     * 
     * @param idTutor
     *     The id_tutor
     */
    public void setIdTutor(String idTutor) {
        this.idTutor = idTutor;
    }

    /**
     * 
     * @return
     *     The idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * 
     * @param idUser
     *     The id_user
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The jk
     */
    public String getJk() {
        return jk;
    }

    /**
     * 
     * @param jk
     *     The jk
     */
    public void setJk(String jk) {
        this.jk = jk;
    }

    /**
     * 
     * @return
     *     The userImage
     */
    public String getUserImage() {
        return userImage;
    }

    /**
     * 
     * @param userImage
     *     The user_image
     */
    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    /**
     * 
     * @return
     *     The schoolName
     */
    public String getSchoolName() {
        return schoolName;
    }

    /**
     * 
     * @param schoolName
     *     The school_name
     */
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    /**
     * 
     * @return
     *     The halaman
     */
    public String getHalaman() {
        return halaman;
    }

    /**
     * 
     * @param halaman
     *     The halaman
     */
    public void setHalaman(String halaman) {
        this.halaman = halaman;
    }

    /**
     * 
     * @return
     *     The surat
     */
    public String getSurat() {
        return surat;
    }

    /**
     * 
     * @param surat
     *     The surat
     */
    public void setSurat(String surat) {
        this.surat = surat;
    }

    /**
     * 
     * @return
     *     The ayat
     */
    public String getAyat() {
        return ayat;
    }

    /**
     * 
     * @param ayat
     *     The ayat
     */
    public void setAyat(String ayat) {
        this.ayat = ayat;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idMengaji);
        dest.writeValue(idTutor);
        dest.writeValue(idUser);
        dest.writeValue(name);
        dest.writeValue(jk);
        dest.writeValue(userImage);
        dest.writeValue(schoolName);
        dest.writeValue(halaman);
        dest.writeValue(surat);
        dest.writeValue(ayat);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return  0;
    }

}
