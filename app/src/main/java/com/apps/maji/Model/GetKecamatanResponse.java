
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetKecamatanResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private List<DataKecamatan> dataKecamatan = new ArrayList<DataKecamatan>();
    public final static Parcelable.Creator<GetKecamatanResponse> CREATOR = new Creator<GetKecamatanResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GetKecamatanResponse createFromParcel(Parcel in) {
            GetKecamatanResponse instance = new GetKecamatanResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.dataKecamatan, (com.apps.maji.Model.DataKecamatan.class.getClassLoader()));
            return instance;
        }

        public GetKecamatanResponse[] newArray(int size) {
            return (new GetKecamatanResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The dataKecamatan
     */
    public List<DataKecamatan> getDataKecamatan() {
        return dataKecamatan;
    }

    /**
     * 
     * @param dataKecamatan
     *     The dataKecamatan
     */
    public void setDataKecamatan(List<DataKecamatan> dataKecamatan) {
        this.dataKecamatan = dataKecamatan;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(code);
        dest.writeList(dataKecamatan);
    }

    public int describeContents() {
        return  0;
    }

}
