
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSekolahResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private List<DataSchool> dataSchool = new ArrayList<DataSchool>();
    public final static Parcelable.Creator<GetSekolahResponse> CREATOR = new Creator<GetSekolahResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GetSekolahResponse createFromParcel(Parcel in) {
            GetSekolahResponse instance = new GetSekolahResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.dataSchool, (com.apps.maji.Model.DataSchool.class.getClassLoader()));
            return instance;
        }

        public GetSekolahResponse[] newArray(int size) {
            return (new GetSekolahResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The dataSchool
     */
    public List<DataSchool> getDataSchool() {
        return dataSchool;
    }

    /**
     * 
     * @param dataSchool
     *     The dataSchool
     */
    public void setDataSchool(List<DataSchool> dataSchool) {
        this.dataSchool = dataSchool;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(code);
        dest.writeList(dataSchool);
    }

    public int describeContents() {
        return  0;
    }

}
