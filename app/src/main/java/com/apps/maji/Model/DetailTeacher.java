
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailTeacher implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("no_hp")
    @Expose
    private String noHp;
    @SerializedName("jk")
    @Expose
    private String jk;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("qrcode")
    @Expose
    private Object qrcode;
    @SerializedName("user_image")
    @Expose
    private Object userImage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_available")
    @Expose
    private String statusAvailable;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    public final static Parcelable.Creator<DetailTeacher> CREATOR = new Creator<DetailTeacher>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DetailTeacher createFromParcel(Parcel in) {
            DetailTeacher instance = new DetailTeacher();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.email = ((String) in.readValue((String.class.getClassLoader())));
            instance.noHp = ((String) in.readValue((String.class.getClassLoader())));
            instance.jk = ((String) in.readValue((String.class.getClassLoader())));
            instance.password = ((String) in.readValue((String.class.getClassLoader())));
            instance.usertype = ((String) in.readValue((String.class.getClassLoader())));
            instance.schoolId = ((String) in.readValue((String.class.getClassLoader())));
            instance.qrcode = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.userImage = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.statusAvailable = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DetailTeacher[] newArray(int size) {
            return (new DetailTeacher[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The noHp
     */
    public String getNoHp() {
        return noHp;
    }

    /**
     * 
     * @param noHp
     *     The no_hp
     */
    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    /**
     * 
     * @return
     *     The jk
     */
    public String getJk() {
        return jk;
    }

    /**
     * 
     * @param jk
     *     The jk
     */
    public void setJk(String jk) {
        this.jk = jk;
    }

    /**
     * 
     * @return
     *     The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password
     *     The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * @return
     *     The usertype
     */
    public String getUsertype() {
        return usertype;
    }

    /**
     * 
     * @param usertype
     *     The usertype
     */
    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    /**
     * 
     * @return
     *     The schoolId
     */
    public String getSchoolId() {
        return schoolId;
    }

    /**
     * 
     * @param schoolId
     *     The school_id
     */
    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    /**
     * 
     * @return
     *     The qrcode
     */
    public Object getQrcode() {
        return qrcode;
    }

    /**
     * 
     * @param qrcode
     *     The qrcode
     */
    public void setQrcode(Object qrcode) {
        this.qrcode = qrcode;
    }

    /**
     * 
     * @return
     *     The userImage
     */
    public Object getUserImage() {
        return userImage;
    }

    /**
     * 
     * @param userImage
     *     The user_image
     */
    public void setUserImage(Object userImage) {
        this.userImage = userImage;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The statusAvailable
     */
    public String getStatusAvailable() {
        return statusAvailable;
    }

    /**
     * 
     * @param statusAvailable
     *     The status_available
     */
    public void setStatusAvailable(String statusAvailable) {
        this.statusAvailable = statusAvailable;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(email);
        dest.writeValue(noHp);
        dest.writeValue(jk);
        dest.writeValue(password);
        dest.writeValue(usertype);
        dest.writeValue(schoolId);
        dest.writeValue(qrcode);
        dest.writeValue(userImage);
        dest.writeValue(status);
        dest.writeValue(statusAvailable);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
    }

    @Override
    public String toString() {
        return "DetailTeacher{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", noHp='" + noHp + '\'' +
                ", jk='" + jk + '\'' +
                ", password='" + password + '\'' +
                ", usertype='" + usertype + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", qrcode=" + qrcode +
                ", userImage=" + userImage +
                ", status='" + status + '\'' +
                ", statusAvailable='" + statusAvailable + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }

    public int describeContents() {
        return  0;
    }

    public String serialize() {
        // Serialize this class into a JSON string using GSON
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    static public DetailTeacher create(String serializedData) {
        // Use GSON to instantiate this class using the JSON representation of the state
        Gson gson = new Gson();
        return gson.fromJson(serializedData, DetailTeacher.class);
    }
}
