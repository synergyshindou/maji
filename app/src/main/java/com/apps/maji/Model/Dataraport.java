
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dataraport implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_tutor")
    @Expose
    private String idTutor;
    @SerializedName("kategori")
    @Expose
    private String kategori;
    @SerializedName("jam_belajar")
    @Expose
    private String jamBelajar;
    @SerializedName("halaman")
    @Expose
    private String halaman;
    @SerializedName("surat")
    @Expose
    private String surat;
    @SerializedName("ayat")
    @Expose
    private String ayat;
    @SerializedName("nilai_konsistensi")
    @Expose
    private String nilaiKonsistensi;
    @SerializedName("nilai_ketelitian")
    @Expose
    private String nilaiKetelitian;
    @SerializedName("nilai_kerapian")
    @Expose
    private String nilaiKerapian;
    @SerializedName("tgl_belajar")
    @Expose
    private String tglBelajar;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("name_guru")
    @Expose
    private String nameGuru;
    @SerializedName("name_peserta")
    @Expose
    private String namePeserta;
    @SerializedName("evaluasi")
    @Expose
    private String evaluasi;
    @SerializedName("comment")
    @Expose
    private String comment;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Dataraport{");
        sb.append("id='").append(id).append('\'');
        sb.append(", idUser='").append(idUser).append('\'');
        sb.append(", idTutor='").append(idTutor).append('\'');
        sb.append(", kategori='").append(kategori).append('\'');
        sb.append(", jamBelajar='").append(jamBelajar).append('\'');
        sb.append(", halaman='").append(halaman).append('\'');
        sb.append(", surat='").append(surat).append('\'');
        sb.append(", ayat='").append(ayat).append('\'');
        sb.append(", nilaiKonsistensi='").append(nilaiKonsistensi).append('\'');
        sb.append(", nilaiKetelitian='").append(nilaiKetelitian).append('\'');
        sb.append(", nilaiKerapian='").append(nilaiKerapian).append('\'');
        sb.append(", tglBelajar='").append(tglBelajar).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append(", nameGuru='").append(nameGuru).append('\'');
        sb.append(", namePeserta='").append(namePeserta).append('\'');
        sb.append(", evaluasi='").append(evaluasi).append('\'');
        sb.append(", comment='").append(comment).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public final static Parcelable.Creator<Dataraport> CREATOR = new Creator<Dataraport>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Dataraport createFromParcel(Parcel in) {
            Dataraport instance = new Dataraport();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.idUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.idTutor = ((String) in.readValue((String.class.getClassLoader())));
            instance.kategori = ((String) in.readValue((String.class.getClassLoader())));
            instance.jamBelajar = ((String) in.readValue((String.class.getClassLoader())));
            instance.halaman = ((String) in.readValue((String.class.getClassLoader())));
            instance.surat = ((String) in.readValue((String.class.getClassLoader())));
            instance.ayat = ((String) in.readValue((String.class.getClassLoader())));
            instance.nilaiKonsistensi = ((String) in.readValue((String.class.getClassLoader())));
            instance.nilaiKetelitian = ((String) in.readValue((String.class.getClassLoader())));
            instance.nilaiKerapian = ((String) in.readValue((String.class.getClassLoader())));
            instance.tglBelajar = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.nameGuru = ((String) in.readValue((String.class.getClassLoader())));
            instance.namePeserta = ((String) in.readValue((String.class.getClassLoader())));
            instance.evaluasi = ((String) in.readValue((String.class.getClassLoader())));
            instance.comment = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Dataraport[] newArray(int size) {
            return (new Dataraport[size]);
        }

    };

    public String getEvaluasi() {
        return evaluasi;
    }

    public void setEvaluasi(String evaluasi) {
        this.evaluasi = evaluasi;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The idTutor
     */
    public String getIdTutor() {
        return idTutor;
    }

    /**
     * @param idTutor The id_tutor
     */
    public void setIdTutor(String idTutor) {
        this.idTutor = idTutor;
    }

    /**
     * @return The kategori
     */
    public String getKategori() {
        return kategori;
    }

    /**
     * @param kategori The kategori
     */
    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    /**
     * @return The jamBelajar
     */
    public String getJamBelajar() {
        return jamBelajar;
    }

    /**
     * @param jamBelajar The jam_belajar
     */
    public void setJamBelajar(String jamBelajar) {
        this.jamBelajar = jamBelajar;
    }

    /**
     * @return The halaman
     */
    public String getHalaman() {
        return halaman;
    }

    /**
     * @param halaman The halaman
     */
    public void setHalaman(String halaman) {
        this.halaman = halaman;
    }

    /**
     * @return The surat
     */
    public String getSurat() {
        return surat;
    }

    /**
     * @param surat The surat
     */
    public void setSurat(String surat) {
        this.surat = surat;
    }

    /**
     * @return The ayat
     */
    public String getAyat() {
        return ayat;
    }

    /**
     * @param ayat The ayat
     */
    public void setAyat(String ayat) {
        this.ayat = ayat;
    }

    /**
     * @return The nilaiKonsistensi
     */
    public String getNilaiKonsistensi() {
        return nilaiKonsistensi;
    }

    /**
     * @param nilaiKonsistensi The nilai_konsistensi
     */
    public void setNilaiKonsistensi(String nilaiKonsistensi) {
        this.nilaiKonsistensi = nilaiKonsistensi;
    }

    /**
     * @return The nilaiKetelitian
     */
    public String getNilaiKetelitian() {
        return nilaiKetelitian;
    }

    /**
     * @param nilaiKetelitian The nilai_ketelitian
     */
    public void setNilaiKetelitian(String nilaiKetelitian) {
        this.nilaiKetelitian = nilaiKetelitian;
    }

    /**
     * @return The nilaiKerapian
     */
    public String getNilaiKerapian() {
        return nilaiKerapian;
    }

    /**
     * @param nilaiKerapian The nilai_kerapian
     */
    public void setNilaiKerapian(String nilaiKerapian) {
        this.nilaiKerapian = nilaiKerapian;
    }

    /**
     * @return The tglBelajar
     */
    public String getTglBelajar() {
        return tglBelajar;
    }

    /**
     * @param tglBelajar The tgl_belajar
     */
    public void setTglBelajar(String tglBelajar) {
        this.tglBelajar = tglBelajar;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The nameGuru
     */
    public String getNameGuru() {
        return nameGuru;
    }

    /**
     * @param nameGuru The name_guru
     */
    public void setNameGuru(String nameGuru) {
        this.nameGuru = nameGuru;
    }

    /**
     * @return The namePeserta
     */
    public String getNamePeserta() {
        return namePeserta;
    }

    /**
     * @param namePeserta The name_peserta
     */
    public void setNamePeserta(String namePeserta) {
        this.namePeserta = namePeserta;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(idUser);
        dest.writeValue(idTutor);
        dest.writeValue(kategori);
        dest.writeValue(jamBelajar);
        dest.writeValue(halaman);
        dest.writeValue(surat);
        dest.writeValue(ayat);
        dest.writeValue(nilaiKonsistensi);
        dest.writeValue(nilaiKetelitian);
        dest.writeValue(nilaiKerapian);
        dest.writeValue(tglBelajar);
        dest.writeValue(createdAt);
        dest.writeValue(nameGuru);
        dest.writeValue(namePeserta);
        dest.writeValue(evaluasi);
        dest.writeValue(comment);
    }

    public int describeContents() {
        return 0;
    }

}
