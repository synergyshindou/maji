
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckTutorResponse implements Parcelable {

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("detailTeacher")
    @Expose
    private DetailTeacher detailTeacher;

    @Override
    public String toString() {
        return "CheckTutorResponse{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", detailTeacher=" + detailTeacher +
                '}';
    }

    public final static Parcelable.Creator<CheckTutorResponse> CREATOR = new Creator<CheckTutorResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckTutorResponse createFromParcel(Parcel in) {
            CheckTutorResponse instance = new CheckTutorResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.detailTeacher = ((DetailTeacher) in.readValue((DetailTeacher.class.getClassLoader())));
            return instance;
        }

        public CheckTutorResponse[] newArray(int size) {
            return (new CheckTutorResponse[size]);
        }

    };

    /**
     * @return The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The detailTeacher
     */
    public DetailTeacher getDetailTeacher() {
        return detailTeacher;
    }

    /**
     * @param detailTeacher The detailTeacher
     */
    public void setDetailTeacher(DetailTeacher detailTeacher) {
        this.detailTeacher = detailTeacher;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(detailTeacher);
    }

    public int describeContents() {
        return 0;
    }

}
