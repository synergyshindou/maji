package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckVersionResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataVersion dataVersion;
    public final static Parcelable.Creator<CheckVersionResponse> CREATOR = new Creator<CheckVersionResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CheckVersionResponse createFromParcel(Parcel in) {
            CheckVersionResponse instance = new CheckVersionResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.dataVersion = ((DataVersion) in.readValue((DataVersion.class.getClassLoader())));
            return instance;
        }

        public CheckVersionResponse[] newArray(int size) {
            return (new CheckVersionResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataVersion
     */
    public DataVersion getDataVersion() {
        return dataVersion;
    }

    /**
     * 
     * @param dataVersion
     *     The dataVersion
     */
    public void setDataVersion(DataVersion dataVersion) {
        this.dataVersion = dataVersion;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeValue(dataVersion);
    }

    public int describeContents() {
        return  0;
    }

    @Override
    public String toString() {
        return "CheckVersionResponse{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", dataVersion=" + dataVersion +
                '}';
    }
}
