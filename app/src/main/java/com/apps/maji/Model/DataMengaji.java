package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataMengaji implements Parcelable
{

    @SerializedName("id_mengaji")
    @Expose
    private String idMengaji;
    @SerializedName("id_tutor")
    @Expose
    private String idTutor;
    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("halaman")
    @Expose
    private String halaman;
    @SerializedName("surat")
    @Expose
    private String surat;
    @SerializedName("ayat")
    @Expose
    private String ayat;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("sound")
    @Expose
    private String sound;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("name")
    @Expose
    private String name;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DataMengaji{");
        sb.append("idMengaji='").append(idMengaji).append('\'');
        sb.append(", idTutor='").append(idTutor).append('\'');
        sb.append(", idUser='").append(idUser).append('\'');
        sb.append(", halaman='").append(halaman).append('\'');
        sb.append(", surat='").append(surat).append('\'');
        sb.append(", ayat='").append(ayat).append('\'');
        sb.append(", image='").append(image).append('\'');
        sb.append(", sound='").append(sound).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public final static Parcelable.Creator<DataMengaji> CREATOR = new Creator<DataMengaji>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataMengaji createFromParcel(Parcel in) {
            DataMengaji instance = new DataMengaji();
            instance.idMengaji = ((String) in.readValue((String.class.getClassLoader())));
            instance.idTutor = ((String) in.readValue((String.class.getClassLoader())));
            instance.idUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.halaman = ((String) in.readValue((String.class.getClassLoader())));
            instance.surat = ((String) in.readValue((String.class.getClassLoader())));
            instance.ayat = ((String) in.readValue((String.class.getClassLoader())));
            instance.image = ((String) in.readValue((String.class.getClassLoader())));
            instance.sound = ((String) in.readValue((String.class.getClassLoader())));
            instance.type = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public DataMengaji[] newArray(int size) {
            return (new DataMengaji[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The idMengaji
     */
    public String getIdMengaji() {
        return idMengaji;
    }

    /**
     * 
     * @param idMengaji
     *     The id_mengaji
     */
    public void setIdMengaji(String idMengaji) {
        this.idMengaji = idMengaji;
    }

    /**
     * 
     * @return
     *     The idTutor
     */
    public String getIdTutor() {
        return idTutor;
    }

    /**
     * 
     * @param idTutor
     *     The id_tutor
     */
    public void setIdTutor(String idTutor) {
        this.idTutor = idTutor;
    }

    /**
     * 
     * @return
     *     The idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * 
     * @param idUser
     *     The id_user
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * 
     * @return
     *     The halaman
     */
    public String getHalaman() {
        return halaman;
    }

    /**
     * 
     * @param halaman
     *     The halaman
     */
    public void setHalaman(String halaman) {
        this.halaman = halaman;
    }

    /**
     * 
     * @return
     *     The surat
     */
    public String getSurat() {
        return surat;
    }

    /**
     * 
     * @param surat
     *     The surat
     */
    public void setSurat(String surat) {
        this.surat = surat;
    }

    /**
     * 
     * @return
     *     The ayat
     */
    public String getAyat() {
        return ayat;
    }

    /**
     * 
     * @param ayat
     *     The ayat
     */
    public void setAyat(String ayat) {
        this.ayat = ayat;
    }

    /**
     * 
     * @return
     *     The image
     */
    public String getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * 
     * @return
     *     The sound
     */
    public String getSound() {
        return sound;
    }

    /**
     * 
     * @param sound
     *     The sound
     */
    public void setSound(String sound) {
        this.sound = sound;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idMengaji);
        dest.writeValue(idTutor);
        dest.writeValue(idUser);
        dest.writeValue(halaman);
        dest.writeValue(surat);
        dest.writeValue(ayat);
        dest.writeValue(image);
        dest.writeValue(sound);
        dest.writeValue(type);
        dest.writeValue(createdAt);
        dest.writeValue(name);
    }

    public int describeContents() {
        return  0;
    }

}
