
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserGroup implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("id_group")
    @Expose
    private int idGroup;
    @SerializedName("id_user")
    @Expose
    private int idUser;
    @SerializedName("user")
    @Expose
    private User user;
    public final static Parcelable.Creator<UserGroup> CREATOR = new Creator<UserGroup>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UserGroup createFromParcel(Parcel in) {
            UserGroup instance = new UserGroup();
            instance.id = ((int) in.readValue((int.class.getClassLoader())));
            instance.idGroup = ((int) in.readValue((int.class.getClassLoader())));
            instance.idUser = ((int) in.readValue((int.class.getClassLoader())));
            instance.user = ((User) in.readValue((User.class.getClassLoader())));
            return instance;
        }

        public UserGroup[] newArray(int size) {
            return (new UserGroup[size]);
        }

    };

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The idGroup
     */
    public int getIdGroup() {
        return idGroup;
    }

    /**
     * @param idGroup The id_group
     */
    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * @return The idUser
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * @param idUser The id_user
     */
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(idGroup);
        dest.writeValue(idUser);
        dest.writeValue(user);
    }

    public int describeContents() {
        return 0;
    }

}
