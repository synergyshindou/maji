
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetKotaResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("data")
    @Expose
    private List<DataKota> dataKota = new ArrayList<DataKota>();
    public final static Parcelable.Creator<GetKotaResponse> CREATOR = new Creator<GetKotaResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public GetKotaResponse createFromParcel(Parcel in) {
            GetKotaResponse instance = new GetKotaResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            in.readList(instance.dataKota, (DataKota.class.getClassLoader()));
            return instance;
        }

        public GetKotaResponse[] newArray(int size) {
            return (new GetKotaResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The dataKota
     */
    public List<DataKota> getDataKota() {
        return dataKota;
    }

    /**
     * 
     * @param dataKota
     *     The dataKota
     */
    public void setDataKota(List<DataKota> dataKota) {
        this.dataKota = dataKota;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(code);
        dest.writeList(dataKota);
    }

    public int describeContents() {
        return  0;
    }

}
