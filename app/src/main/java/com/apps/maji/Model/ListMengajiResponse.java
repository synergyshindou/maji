package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListMengajiResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DataMengaji> dataMengaji = new ArrayList<DataMengaji>();

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ListMengajiResponse{");
        sb.append("status=").append(status);
        sb.append(", code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append(", dataMengaji=").append(dataMengaji);
        sb.append('}');
        return sb.toString();
    }

    public final static Parcelable.Creator<ListMengajiResponse> CREATOR = new Creator<ListMengajiResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ListMengajiResponse createFromParcel(Parcel in) {
            ListMengajiResponse instance = new ListMengajiResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.dataMengaji, (com.apps.maji.Model.DataMengaji.class.getClassLoader()));
            return instance;
        }

        public ListMengajiResponse[] newArray(int size) {
            return (new ListMengajiResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataMengaji
     */
    public List<DataMengaji> getDataMengaji() {
        return dataMengaji;
    }

    /**
     * 
     * @param dataMengaji
     *     The dataMengaji
     */
    public void setDataMengaji(List<DataMengaji> dataMengaji) {
        this.dataMengaji = dataMengaji;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeList(dataMengaji);
    }

    public int describeContents() {
        return  0;
    }

}
