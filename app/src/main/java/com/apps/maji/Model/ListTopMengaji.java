
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListTopMengaji implements Parcelable {

    @Override
    public String toString() {
        return "ListTopMengaji{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", dataTopMengaji=" + dataTopMengaji +
                '}';
    }

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DataTopMengaji> dataTopMengaji = new ArrayList<DataTopMengaji>();
    public final static Parcelable.Creator<ListTopMengaji> CREATOR = new Creator<ListTopMengaji>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ListTopMengaji createFromParcel(Parcel in) {
            ListTopMengaji instance = new ListTopMengaji();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.dataTopMengaji, (com.apps.maji.Model.DataTopMengaji.class.getClassLoader()));
            return instance;
        }

        public ListTopMengaji[] newArray(int size) {
            return (new ListTopMengaji[size]);
        }

    };

    /**
     * @return The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The dataTopMengaji
     */
    public List<DataTopMengaji> getDataTopMengaji() {
        return dataTopMengaji;
    }

    /**
     * @param dataTopMengaji The dataTopMengaji
     */
    public void setDataTopMengaji(List<DataTopMengaji> dataTopMengaji) {
        this.dataTopMengaji = dataTopMengaji;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeList(dataTopMengaji);
    }

    public int describeContents() {
        return 0;
    }

}
