
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageUnread implements Parcelable
{

    @Override
    public String toString() {
        return "MessageUnread{" +
                "idGroup=" + idGroup +
                ", nameGroup='" + nameGroup + '\'' +
                ", lastMessage='" + lastMessage + '\'' +
                ", lastUser='" + lastUser + '\'' +
                ", unread=" + unread +
                '}';
    }

    @SerializedName("id_group")
    @Expose
    private int idGroup;
    @SerializedName("name_group")
    @Expose
    private String nameGroup;
    @SerializedName("last_message")
    @Expose
    private String lastMessage;
    @SerializedName("last_user")
    @Expose
    private String lastUser;
    @SerializedName("unread")
    @Expose
    private int unread;
    public final static Creator<MessageUnread> CREATOR = new Creator<MessageUnread>() {


        @SuppressWarnings({
            "unchecked"
        })
        public MessageUnread createFromParcel(Parcel in) {
            MessageUnread instance = new MessageUnread();
            instance.idGroup = ((int) in.readValue((int.class.getClassLoader())));
            instance.nameGroup = ((String) in.readValue((String.class.getClassLoader())));
            instance.lastMessage = ((String) in.readValue((String.class.getClassLoader())));
            instance.lastUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.unread = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public MessageUnread[] newArray(int size) {
            return (new MessageUnread[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The idGroup
     */
    public int getIdGroup() {
        return idGroup;
    }

    /**
     * 
     * @param idGroup
     *     The id_group
     */
    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * 
     * @return
     *     The nameGroup
     */
    public String getNameGroup() {
        return nameGroup;
    }

    /**
     * 
     * @param nameGroup
     *     The name_group
     */
    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    /**
     * 
     * @return
     *     The lastMessage
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * 
     * @param lastMessage
     *     The last_message
     */
    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    /**
     * 
     * @return
     *     The lastUser
     */
    public String getLastUser() {
        return lastUser;
    }

    /**
     * 
     * @param lastUser
     *     The last_user
     */
    public void setLastUser(String lastUser) {
        this.lastUser = lastUser;
    }

    /**
     * 
     * @return
     *     The unread
     */
    public int getUnread() {
        return unread;
    }

    /**
     * 
     * @param unread
     *     The unread
     */
    public void setUnread(int unread) {
        this.unread = unread;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idGroup);
        dest.writeValue(nameGroup);
        dest.writeValue(lastMessage);
        dest.writeValue(lastUser);
        dest.writeValue(unread);
    }

    public int describeContents() {
        return  0;
    }

}
