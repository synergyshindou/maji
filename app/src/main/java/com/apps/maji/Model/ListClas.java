
package com.apps.maji.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListClas implements Parcelable
{

    @SerializedName("class_id")
    @Expose
    private String classId;
    @SerializedName("subject_id")
    @Expose
    private String subjectId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("urutan")
    @Expose
    private String urutan;
    @SerializedName("topic_plan")
    @Expose
    private String topicPlan;
    @SerializedName("topic_actual")
    @Expose
    private String topicActual;
    @SerializedName("tutor_id")
    @Expose
    private String tutorId;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("finish_time")
    @Expose
    private String finishTime;
    @SerializedName("last_order")
    @Expose
    private Object lastOrder;
    @SerializedName("duration")
    @Expose
    private Object duration;
    @SerializedName("participant")
    @Expose
    private Participant participant;
    @SerializedName("max_participants")
    @Expose
    private String maxParticipants;
    @SerializedName("class_type")
    @Expose
    private String classType;
    @SerializedName("template_type")
    @Expose
    private String templateType;
    @SerializedName("channel_id")
    @Expose
    private String channelId;
    @SerializedName("channel_status")
    @Expose
    private Object channelStatus;
    @SerializedName("break_state")
    @Expose
    private String breakState;
    @SerializedName("break_pos")
    @Expose
    private String breakPos;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ListClas{");
        sb.append("classId='").append(classId).append('\'');
        sb.append(", subjectId='").append(subjectId).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", icon='").append(icon).append('\'');
        sb.append(", urutan='").append(urutan).append('\'');
        sb.append(", topicPlan='").append(topicPlan).append('\'');
        sb.append(", topicActual='").append(topicActual).append('\'');
        sb.append(", tutorId='").append(tutorId).append('\'');
        sb.append(", startTime='").append(startTime).append('\'');
        sb.append(", finishTime='").append(finishTime).append('\'');
        sb.append(", lastOrder=").append(lastOrder);
        sb.append(", duration=").append(duration);
        sb.append(", participant=").append(participant);
        sb.append(", maxParticipants='").append(maxParticipants).append('\'');
        sb.append(", classType='").append(classType).append('\'');
        sb.append(", templateType='").append(templateType).append('\'');
        sb.append(", channelId='").append(channelId).append('\'');
        sb.append(", channelStatus=").append(channelStatus);
        sb.append(", breakState='").append(breakState).append('\'');
        sb.append(", breakPos='").append(breakPos).append('\'');
        sb.append(", createdAt='").append(createdAt).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public final static Parcelable.Creator<ListClas> CREATOR = new Creator<ListClas>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ListClas createFromParcel(Parcel in) {
            ListClas instance = new ListClas();
            instance.classId = ((String) in.readValue((String.class.getClassLoader())));
            instance.subjectId = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.description = ((String) in.readValue((String.class.getClassLoader())));
            instance.icon = ((String) in.readValue((String.class.getClassLoader())));
            instance.urutan = ((String) in.readValue((String.class.getClassLoader())));
            instance.topicPlan = ((String) in.readValue((String.class.getClassLoader())));
            instance.topicActual = ((String) in.readValue((String.class.getClassLoader())));
            instance.tutorId = ((String) in.readValue((String.class.getClassLoader())));
            instance.startTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.finishTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.lastOrder = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.duration = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.participant = ((Participant) in.readValue((Participant.class.getClassLoader())));
            instance.maxParticipants = ((String) in.readValue((String.class.getClassLoader())));
            instance.classType = ((String) in.readValue((String.class.getClassLoader())));
            instance.templateType = ((String) in.readValue((String.class.getClassLoader())));
            instance.channelId = ((String) in.readValue((String.class.getClassLoader())));
            instance.channelStatus = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.breakState = ((String) in.readValue((String.class.getClassLoader())));
            instance.breakPos = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ListClas[] newArray(int size) {
            return (new ListClas[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The classId
     */
    public String getClassId() {
        return classId;
    }

    /**
     * 
     * @param classId
     *     The class_id
     */
    public void setClassId(String classId) {
        this.classId = classId;
    }

    /**
     * 
     * @return
     *     The subjectId
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * 
     * @param subjectId
     *     The subject_id
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * 
     * @param description
     *     The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return
     *     The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * 
     * @param icon
     *     The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 
     * @return
     *     The urutan
     */
    public String getUrutan() {
        return urutan;
    }

    /**
     * 
     * @param urutan
     *     The urutan
     */
    public void setUrutan(String urutan) {
        this.urutan = urutan;
    }

    /**
     * 
     * @return
     *     The topicPlan
     */
    public String getTopicPlan() {
        return topicPlan;
    }

    /**
     * 
     * @param topicPlan
     *     The topic_plan
     */
    public void setTopicPlan(String topicPlan) {
        this.topicPlan = topicPlan;
    }

    /**
     * 
     * @return
     *     The topicActual
     */
    public String getTopicActual() {
        return topicActual;
    }

    /**
     * 
     * @param topicActual
     *     The topic_actual
     */
    public void setTopicActual(String topicActual) {
        this.topicActual = topicActual;
    }

    /**
     * 
     * @return
     *     The tutorId
     */
    public String getTutorId() {
        return tutorId;
    }

    /**
     * 
     * @param tutorId
     *     The tutor_id
     */
    public void setTutorId(String tutorId) {
        this.tutorId = tutorId;
    }

    /**
     * 
     * @return
     *     The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * 
     * @param startTime
     *     The start_time
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * 
     * @return
     *     The finishTime
     */
    public String getFinishTime() {
        return finishTime;
    }

    /**
     * 
     * @param finishTime
     *     The finish_time
     */
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    /**
     * 
     * @return
     *     The lastOrder
     */
    public Object getLastOrder() {
        return lastOrder;
    }

    /**
     * 
     * @param lastOrder
     *     The last_order
     */
    public void setLastOrder(Object lastOrder) {
        this.lastOrder = lastOrder;
    }

    /**
     * 
     * @return
     *     The duration
     */
    public Object getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    public void setDuration(Object duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The participant
     */
    public Participant getParticipant() {
        return participant;
    }

    /**
     * 
     * @param participant
     *     The participant
     */
    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    /**
     * 
     * @return
     *     The maxParticipants
     */
    public String getMaxParticipants() {
        return maxParticipants;
    }

    /**
     * 
     * @param maxParticipants
     *     The max_participants
     */
    public void setMaxParticipants(String maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    /**
     * 
     * @return
     *     The classType
     */
    public String getClassType() {
        return classType;
    }

    /**
     * 
     * @param classType
     *     The class_type
     */
    public void setClassType(String classType) {
        this.classType = classType;
    }

    /**
     * 
     * @return
     *     The templateType
     */
    public String getTemplateType() {
        return templateType;
    }

    /**
     * 
     * @param templateType
     *     The template_type
     */
    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    /**
     * 
     * @return
     *     The channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * 
     * @param channelId
     *     The channel_id
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * 
     * @return
     *     The channelStatus
     */
    public Object getChannelStatus() {
        return channelStatus;
    }

    /**
     * 
     * @param channelStatus
     *     The channel_status
     */
    public void setChannelStatus(Object channelStatus) {
        this.channelStatus = channelStatus;
    }

    /**
     * 
     * @return
     *     The breakState
     */
    public String getBreakState() {
        return breakState;
    }

    /**
     * 
     * @param breakState
     *     The break_state
     */
    public void setBreakState(String breakState) {
        this.breakState = breakState;
    }

    /**
     * 
     * @return
     *     The breakPos
     */
    public String getBreakPos() {
        return breakPos;
    }

    /**
     * 
     * @param breakPos
     *     The break_pos
     */
    public void setBreakPos(String breakPos) {
        this.breakPos = breakPos;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(classId);
        dest.writeValue(subjectId);
        dest.writeValue(name);
        dest.writeValue(description);
        dest.writeValue(icon);
        dest.writeValue(urutan);
        dest.writeValue(topicPlan);
        dest.writeValue(topicActual);
        dest.writeValue(tutorId);
        dest.writeValue(startTime);
        dest.writeValue(finishTime);
        dest.writeValue(lastOrder);
        dest.writeValue(duration);
        dest.writeValue(participant);
        dest.writeValue(maxParticipants);
        dest.writeValue(classType);
        dest.writeValue(templateType);
        dest.writeValue(channelId);
        dest.writeValue(channelStatus);
        dest.writeValue(breakState);
        dest.writeValue(breakPos);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return  0;
    }

}
