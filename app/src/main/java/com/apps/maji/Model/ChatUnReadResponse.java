
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatUnReadResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("messages")
    @Expose
    private List<MessagesUnread> messagesUnread = new ArrayList<MessagesUnread>();
    public final static Parcelable.Creator<ChatUnReadResponse> CREATOR = new Creator<ChatUnReadResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ChatUnReadResponse createFromParcel(Parcel in) {
            ChatUnReadResponse instance = new ChatUnReadResponse();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.messagesUnread, (com.apps.maji.Model.MessagesUnread.class.getClassLoader()));
            return instance;
        }

        public ChatUnReadResponse[] newArray(int size) {
            return (new ChatUnReadResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The messagesUnread
     */
    public List<MessagesUnread> getMessagesUnread() {
        return messagesUnread;
    }

    /**
     * 
     * @param messagesUnread
     *     The messagesUnread
     */
    public void setMessagesUnread(List<MessagesUnread> messagesUnread) {
        this.messagesUnread = messagesUnread;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(messagesUnread);
    }

    public int describeContents() {
        return  0;
    }

}
