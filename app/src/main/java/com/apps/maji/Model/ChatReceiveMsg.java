
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatReceiveMsg implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("messages")
    @Expose
    private List<MessagesChat> messagesChat = new ArrayList<MessagesChat>();
    public final static Parcelable.Creator<ChatReceiveMsg> CREATOR = new Creator<ChatReceiveMsg>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ChatReceiveMsg createFromParcel(Parcel in) {
            ChatReceiveMsg instance = new ChatReceiveMsg();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.messagesChat, (com.apps.maji.Model.MessagesChat.class.getClassLoader()));
            return instance;
        }

        public ChatReceiveMsg[] newArray(int size) {
            return (new ChatReceiveMsg[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The messagesChat
     */
    public List<MessagesChat> getMessagesChat() {
        return messagesChat;
    }

    /**
     * 
     * @param messagesChat
     *     The messagesChat
     */
    public void setMessagesChat(List<MessagesChat> messagesChat) {
        this.messagesChat = messagesChat;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(messagesChat);
    }

    public int describeContents() {
        return  0;
    }

}
