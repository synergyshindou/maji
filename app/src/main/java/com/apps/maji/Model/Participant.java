
package com.apps.maji.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Participant implements Parcelable
{

    @SerializedName("visible")
    @Expose
    private String visible;
    @SerializedName("participant")
    @Expose
    private List<Participant_> participant = new ArrayList<Participant_>();
    public final static Parcelable.Creator<Participant> CREATOR = new Creator<Participant>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Participant createFromParcel(Parcel in) {
            Participant instance = new Participant();
            instance.visible = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.participant, (com.apps.maji.Model.Participant_.class.getClassLoader()));
            return instance;
        }

        public Participant[] newArray(int size) {
            return (new Participant[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The visible
     */
    public String getVisible() {
        return visible;
    }

    /**
     * 
     * @param visible
     *     The visible
     */
    public void setVisible(String visible) {
        this.visible = visible;
    }

    /**
     * 
     * @return
     *     The participant
     */
    public List<Participant_> getParticipant() {
        return participant;
    }

    /**
     * 
     * @param participant
     *     The participant
     */
    public void setParticipant(List<Participant_> participant) {
        this.participant = participant;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(visible);
        dest.writeList(participant);
    }

    public int describeContents() {
        return  0;
    }

}
