 package com.apps.maji;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.apps.maji.Utils.Constant.QRID;

 public class QRScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
     private ZXingScannerView mScannerView;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         mScannerView = new ZXingScannerView(this);
         setContentView(mScannerView);
     }
     @Override
     public void onResume() {
         super.onResume();
         mScannerView.setResultHandler(this);
         mScannerView.startCamera();
     }

     @Override
     public void onPause() {
         super.onPause();
         mScannerView.stopCamera();
     }

     @Override
     public void handleResult(Result rawResult) {
         Log.v("TAG", rawResult.getText()); // Prints scan results
         Log.v("TAG", rawResult.getBarcodeFormat().toString());
         AlertDialog.Builder builder = new AlertDialog.Builder(this);
         builder.setTitle("Scan Result");
         builder.setMessage(rawResult.getText());
         AlertDialog alert1 = builder.create();
//         alert1.show();
         Intent data = new Intent();
         String text = rawResult.getText();
         //---set the data to pass back---
         data.putExtra(QRID, text);
         setResult(RESULT_OK, data);
         //---close the activity---
         finish();
         mScannerView.resumeCameraPreview(this);
     }

 }
