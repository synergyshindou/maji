package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.apps.maji.Model.Dataraport;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class NilaiAdapter extends RecyclerView.Adapter<NilaiAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "NilaiAdapter";

    public interface OnItemClickListener {
        void onItemClick(Dataraport item);
    }

    private final OnItemClickListener listener;

    private List<Dataraport> horizontalList;
    private List<Dataraport> orig;
    private Context context;
    CustomFilter filter;

    public NilaiAdapter(Context context, List<Dataraport> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<Dataraport> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getHalaman().toUpperCase().contains(constraint)) {
                        Dataraport r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<Dataraport>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_nilai, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvType, tvTgl, tvGuru, tvHalaman, tvKons, tvKete, tvKera, tvRata, tvEvaluasi;

        MyViewHolder(View view) {
            super(view);
            tvType = view.findViewById(R.id.tvType);
            tvTgl = view.findViewById(R.id.tvTgl);
            tvGuru = view.findViewById(R.id.tvGuru);
            tvHalaman = view.findViewById(R.id.tvHalaman);
            tvKons = view.findViewById(R.id.tvKons);
            tvKete = view.findViewById(R.id.tvKete);
            tvKera = view.findViewById(R.id.tvKera);
            tvRata = view.findViewById(R.id.tvRata);
            tvEvaluasi = view.findViewById(R.id.tvEvaluasi);

        }

        void bind(final Dataraport item, final OnItemClickListener listener) {

            int a = Integer.parseInt(item.getNilaiKonsistensi());
            int b = Integer.parseInt(item.getNilaiKetelitian());
            int c = Integer.parseInt(item.getNilaiKerapian());
            float rata = (a + b + c) / 3;
            tvTgl.setText(Method.FormatDate(item.getTglBelajar() + " " + item.getJamBelajar()));
            tvGuru.setText("Guru " + item.getNameGuru());
            tvHalaman.setText(item.getHalaman() + " - " + item.getSurat() + " " + item.getAyat());

            tvKons.setText(item.getNilaiKonsistensi());
            tvKete.setText(item.getNilaiKetelitian());
            tvKera.setText(item.getNilaiKerapian());
            tvRata.setText("Nilai rata-rata: " + String.valueOf(rata));

            if (item.getEvaluasi() != null){
                Log.i(TAG, "bind: "+item.getId());
                Log.w(TAG, "bind: "+item.getEvaluasi());
                tvEvaluasi.setVisibility(View.VISIBLE);
            } else {
                tvEvaluasi.setVisibility(View.GONE);
            }

            tvEvaluasi.setText(item.getEvaluasi() + "\n" + item.getComment());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
