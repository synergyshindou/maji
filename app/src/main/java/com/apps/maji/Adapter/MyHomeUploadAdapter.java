package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.maji.GlideApp;
import com.apps.maji.Model.DataTopMengaji;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class MyHomeUploadAdapter extends RecyclerView.Adapter<MyHomeUploadAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(DataTopMengaji item);
    }

    private final OnItemClickListener listener;

    private List<DataTopMengaji> horizontalList;
    private List<DataTopMengaji> orig;
    private Context context;
    CustomFilter filter;

    public MyHomeUploadAdapter(Context context, List<DataTopMengaji> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<DataTopMengaji> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getHalaman().toUpperCase().contains(constraint)) {
                        DataTopMengaji r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<DataTopMengaji>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_topupload, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);
        DataTopMengaji item = horizontalList.get(position);
        int im;
        if (item.getJk().equals("L"))
            im = R.drawable.icon_man;
        else
            im = R.drawable.icon_girl;
        GlideApp.with(context)
                .load(item.getUserImage())
                .override(48, 48)
                .placeholder(im)
                .into(holder.img);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvNama, tvSekolah, tvSurah, tvTgl, tvHalaman;
        ImageView img;

        MyViewHolder(View view) {
            super(view);
            tvTgl = view.findViewById(R.id.tvJam);
            tvHalaman = view.findViewById(R.id.tvHalaman);
            tvNama = view.findViewById(R.id.tvNama);
            tvSekolah = view.findViewById(R.id.tvSekolah);
            tvSurah= view.findViewById(R.id.tvSurah);

            img = view.findViewById(R.id.imgMy);
        }

        void bind(final DataTopMengaji item, final OnItemClickListener listener) {

            tvTgl.setText(Method.getDisplayableTime(item.getCreatedAt()));
            tvHalaman.setText("Ayat "+item.getAyat()+ " "+item.getHalaman());
            tvSurah.setText(item.getSurat());
            tvNama.setText(item.getName());
            tvSekolah.setText(item.getSchoolName());


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
