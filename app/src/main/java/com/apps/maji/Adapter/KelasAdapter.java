package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.apps.maji.Model.Data;
import com.apps.maji.Model.ListClas;
import com.apps.maji.Model.Participant;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class KelasAdapter extends RecyclerView.Adapter<KelasAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "KelasAdapter";
    public interface OnItemClickListener {
        void onItemClick(ListClas item);
        void onItemClick(ListClas item, String w);
    }

    private final OnItemClickListener listener;

    private List<ListClas> horizontalList;
    private List<ListClas> orig;
    private Context context;
    CustomFilter filter;

    public KelasAdapter(Context context, List<ListClas> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<ListClas> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getClassId().toUpperCase().contains(constraint)) {
                        ListClas r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<ListClas>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitemscheduleone, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSub,tvDesc,tvTime, tvDate, tvTitle, tvJam;
        Button btnWait, btnEnter, btnJoin, btnPass;

        MyViewHolder(View view) {
            super(view);
            tvSub = view.findViewById(R.id.sub);
            tvDesc = view.findViewById(R.id.desc);
            tvTime = view.findViewById(R.id.time);
            tvDate = view.findViewById(R.id.times);

            btnEnter = view.findViewById(R.id.buttonEnter);
            btnWait = view.findViewById(R.id.buttonEnterWait);
            btnPass = view.findViewById(R.id.buttonEnterFalse);
            btnJoin = view.findViewById(R.id.buttonEnterJoin);


        }

        void bind(final ListClas item, final OnItemClickListener listener) {
            SessionManager sessionManager = new SessionManager(context);
            String d = sessionManager.getData();
            Data myData = Data.create(d);
            String myID = myData.getId();
            Participant p = item.getParticipant();

            if (p.getParticipant() != null) {
                Log.i(TAG, "bind: ADA PAR");
                if (p.getParticipant().size() > 0) {
                    Log.d(TAG, "bind: PAR > 0");
                    for (int i = 0; i < p.getParticipant().size(); i++) {
                        if (p.getParticipant().get(i).getIdUser().equals(myID)){
                            btnEnter.setVisibility(View.VISIBLE);
                            break;
                        }
                    }
                } else {
                    Log.d(TAG, "bind: PAR = 0");
                    btnJoin.setVisibility(View.VISIBLE);
                }
            } else {
                Log.i(TAG, "bind: NO PAR");
                btnJoin.setVisibility(View.VISIBLE);
            }

            tvSub.setText(item.getName());
            tvDesc.setText(item.getDescription());
            tvTime.setText(Method.FormatJamOnly(item.getStartTime())+ " - "+Method.FormatJamOnly(item.getFinishTime()));
            tvDate.setText(Method.FormatDateOnly(item.getStartTime()));
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
            btnJoin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, "join");
                }
            });
            btnEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item, "enter");
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
