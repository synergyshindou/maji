package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.maji.Model.Datum;
import com.apps.maji.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class ListStudentAdapter extends RecyclerView.Adapter<ListStudentAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(Datum item);
    }

    private final OnItemClickListener listener;

    private List<Datum> horizontalList;
    private List<Datum> orig;
    private Context context;
    CustomFilter filter;

    public ListStudentAdapter(Context context, List<Datum> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<Datum> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getNameStudent().toUpperCase().contains(constraint)) {
                        Datum r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<Datum>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_student, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);
        Datum item = horizontalList.get(position);
        int im;
        if (item.getJk().equals("L"))
            im = R.drawable.icon_man;
        else
            im = R.drawable.icon_girl;

        Glide.with(context)
                .load(item.getUserImage())
                .apply(new RequestOptions().placeholder(im))
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvKelas;
        ImageView img;

        MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.name);
            tvKelas = view.findViewById(R.id.kelas);
            img = view.findViewById(R.id.imgMy);

        }

        void bind(final Datum item, final OnItemClickListener listener) {

            tvName.setText(item.getNameStudent());
            if (item.getKelas() != null)
                tvKelas.setText("Kelas "+item.getKelas());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
