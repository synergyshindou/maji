package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.maji.Model.Dataraport;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class MyHomeAdapter extends RecyclerView.Adapter<MyHomeAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(Dataraport item);
    }

    private final OnItemClickListener listener;

    private List<Dataraport> horizontalList;
    private List<Dataraport> orig;
    private Context context;
    CustomFilter filter;

    public MyHomeAdapter(Context context, List<Dataraport> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<Dataraport> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getHalaman().toUpperCase().contains(constraint)) {
                        Dataraport r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<Dataraport>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvType, tvTgl, tvGuru, tvHalaman, tvKons, tvKete, tvKera;

        MyViewHolder(View view) {
            super(view);
            tvType = view.findViewById(R.id.tvType);
            tvTgl = view.findViewById(R.id.tvJam);
            tvGuru = view.findViewById(R.id.tvGuru);
            tvHalaman = view.findViewById(R.id.tvHalaman);
            tvKons = view.findViewById(R.id.tvKonsistensi);
            tvKete = view.findViewById(R.id.tvKetelitian);
            tvKera = view.findViewById(R.id.tvKerapian);

        }

        void bind(final Dataraport item, final OnItemClickListener listener) {
            if (item.getKategori().equals("mandiri"))
                tvType.setText("Mengaji Mandiri");
            if (item.getKategori().equals("guru"))
                tvType.setText("Mengaji dengan Guru");
            if (item.getKategori().equals("online"))
                tvType.setText("Mengaji Online");

            tvTgl.setText(Method.FormatDate(item.getTglBelajar()+" "+item.getJamBelajar()));
            tvGuru.setText(item.getNameGuru());
            tvHalaman.setText(item.getHalaman()+" - "+item.getSurat() +" "+item.getAyat());

            tvKons.setText(item.getNilaiKonsistensi());
            tvKete.setText(item.getNilaiKetelitian());
            tvKera.setText(item.getNilaiKerapian());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
