package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.apps.maji.Model.DataMengaji;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class MengajiAdapter extends RecyclerView.Adapter<MengajiAdapter.MyViewHolder> implements Filterable {

    public interface OnItemClickListener {
        void onItemClick(DataMengaji item);
    }

    private final OnItemClickListener listener;

    private List<DataMengaji> horizontalList;
    private List<DataMengaji> orig;
    private Context context;
    CustomFilter filter;

    public MengajiAdapter(Context context, List<DataMengaji> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<DataMengaji> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getHalaman().toUpperCase().contains(constraint)) {
                        DataMengaji r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<DataMengaji>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_mengaji, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvHari,tvBln,tvThn, tvGuru, tvHalaman, tvSurah, tvAyat, tvJam;

        MyViewHolder(View view) {
            super(view);
            tvHari = view.findViewById(R.id.tvHari);
            tvBln = view.findViewById(R.id.tvBulan);
            tvThn = view.findViewById(R.id.tvTahun);
            tvJam = view.findViewById(R.id.tvJam);
//            tvGuru = view.findViewById(R.id.tvGuru);
            tvHalaman = view.findViewById(R.id.tvHalaman);
            tvSurah = view.findViewById(R.id.tvSurah);
            tvAyat = view.findViewById(R.id.tvAyat);

        }

        void bind(final DataMengaji item, final OnItemClickListener listener) {

            tvHari.setText(Method.FormatHariOnly(item.getCreatedAt()));
            tvBln.setText(Method.FormatTglBlnOnly(item.getCreatedAt()));
            tvThn.setText(Method.FormatThnOnly(item.getCreatedAt()));
            tvJam.setText(Method.FormatJamOnly(item.getCreatedAt()));
//            tvGuru.setText(item.getName());
//            tvHalaman.setText(item.getHalaman()+" - "+item.getSurat() +" "+item.getAyat());
            tvHalaman.setText(item.getHalaman());
            tvSurah.setText(item.getSurat());
            tvAyat.setText(item.getAyat());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
