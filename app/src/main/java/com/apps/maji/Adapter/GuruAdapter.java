package com.apps.maji.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.maji.GlideApp;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataGuru;
import com.apps.maji.Model.Participant;
import com.apps.maji.R;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell_Cleva on 04/01/2017.
 */

public class GuruAdapter extends RecyclerView.Adapter<GuruAdapter.MyViewHolder> implements Filterable {

    private static final String TAG = "KelasAdapter";

    public interface OnItemClickListener {
        void onItemClick(DataGuru item);

        void onItemClick(DataGuru item, String w);
    }

    private final OnItemClickListener listener;

    private List<DataGuru> horizontalList;
    private List<DataGuru> orig;
    private Context context;
    CustomFilter filter;

    public GuruAdapter(Context context, List<DataGuru> horizontalList, OnItemClickListener listener) {
        this.horizontalList = horizontalList;
        this.orig = horizontalList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Filter getFilter() {

        if (filter == null)
            filter = new CustomFilter();

        return filter;
    }

    class CustomFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {

                constraint = constraint.toString().toUpperCase();

                List<DataGuru> filters = new ArrayList<>();

                for (int i = 0; i < orig.size(); i++) {
                    if (orig.get(i).getName().toUpperCase().contains(constraint)) {
                        DataGuru r = orig.get(i);
                        filters.add(r);
                    }
                }

                results.count = filters.size();
                results.values = filters;

            } else {
                results.count = orig.size();
                results.values = orig;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults result) {

            horizontalList = (ArrayList<DataGuru>) result.values;
            notifyDataSetChanged();

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rowtutor_card_baru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.bind(horizontalList.get(position), listener);

    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView img;

        MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.textViewNameC);
            img = view.findViewById(R.id.imageViewC);


        }

        void bind(final DataGuru item, final OnItemClickListener listener) {

            tvName.setText(item.getName());

            GlideApp.with(context)
                    .load(item.getUser_image())
                    .apply(RequestOptions.circleCropTransform())
                    .into(img);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });

//            bg.setBackgroundColor(Color.parseColor(item.getsChannelColor()));
        }
    }
}
