package com.apps.maji.Rest;

import com.apps.maji.Model.ChatAddUserResponse;
import com.apps.maji.Model.ChatGroupCreateResponse;
import com.apps.maji.Model.ChatLoginResponse;
import com.apps.maji.Model.ChatReceiveMsg;
import com.apps.maji.Model.ChatSendMsgResp;
import com.apps.maji.Model.ChatUnReadResponse;
import com.apps.maji.Model.ChatUnreadMsg;
import com.apps.maji.Model.CheckTutorResponse;
import com.apps.maji.Model.CheckVersionResponse;
import com.apps.maji.Model.GetKecamatanResponse;
import com.apps.maji.Model.GetKotaResponse;
import com.apps.maji.Model.GetProvinsiResponse;
import com.apps.maji.Model.GetSekolahResponse;
import com.apps.maji.Model.ListClassResponse;
import com.apps.maji.Model.ListGuruResponse;
import com.apps.maji.Model.ListMengajiResponse;
import com.apps.maji.Model.ListPesertaResponse;
import com.apps.maji.Model.ListRaportResponse;
import com.apps.maji.Model.ListTopMengaji;
import com.apps.maji.Model.LoginResponse;
import com.apps.maji.Model.ProfileResponse;
import com.apps.maji.Model.RegisterResponse;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Model.UserCreateResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiConfig {

    @FormUrlEncoded
    @POST("AuthLogin")
    Call<LoginResponse> login(@Field("name") String name,
                              @Field("password") String pass);

    @FormUrlEncoded
    @POST("List_RaportUserByPeserta/access_token/{access_token}")
    Call<ListRaportResponse> homeraport(@Path("access_token") String token,
                                        @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST("List_MengajiUserByPeserta/access_token/{access_token}")
    Call<ListMengajiResponse> riwayatMengaji(@Path("access_token") String token,
                                             @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST("RegisterAccount")
    Call<ServerResponse> register(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("name") String name,
                                  @Field("jenis_kelamin") String jk,
                                  @Field("no_hp") String no_hp,
                                  @Field("usertype") String type,
                                  @Field("school_id") String schools,
                                  @Field("qrcode") String qrcode);

    @FormUrlEncoded
    @POST("Join_Class")
    Call<ServerResponse> joinClass(@Field("class_id") String classid,
                                   @Field("id_user") String iduser,
                                   @Field("name") String name,
                                   @Field("email") String email,
                                   @Field("usertype") String usertype);

    @Multipart
    @POST("PesertaMengaji/access_token/{access_token}")
    Call<ServerResponse> mengaji(@Path("access_token") String token,
                                 @Part("id_user") RequestBody iduser,
                                 @Part("id_tutor") RequestBody idtutor,
                                 @Part("halaman") RequestBody halaman,
                                 @Part("surat") RequestBody surat,
                                 @Part("ayat") RequestBody ayat,
                                 @Part("page") RequestBody page,
                                 @Part("type") RequestBody type,
                                 @Part MultipartBody.Part image,
                                 @Part MultipartBody.Part sound);
//                                 @PartMap Map<String, RequestBody> map);

    @Multipart
    @POST("checkMengaji/access_token/{access_token}")
    Call<ServerResponse> checkmengaji(@Path("access_token") String token,
                                      @Part("id_user") RequestBody iduser,
                                      @Part("id_tutor") RequestBody idtutor,
                                      @Part("halaman") RequestBody halaman,
                                      @Part("surat") RequestBody surat,
                                      @Part("ayat") RequestBody ayat,
                                      @Part("page") RequestBody page,
                                      @Part("type") RequestBody type);

    @FormUrlEncoded
    @POST("List_Class")
    Call<ListClassResponse> myClass(@Field("id_user") String id_user);

    @GET("List_Guru/access_token/{access_token}")
    Call<ListGuruResponse> getGuru(@Path("access_token") String token);

    @FormUrlEncoded
    @POST("getSchoolDLL")
    Call<GetProvinsiResponse> getProvinsi(@Field("type") String name);

    @FormUrlEncoded
    @POST("getSchoolDLL")
    Call<GetKotaResponse> getKota(@Field("type") String name,
                                  @Field("provinsi_id") String prov);

    @FormUrlEncoded
    @POST("getSchoolDLL")
    Call<GetKecamatanResponse> getKec(@Field("type") String name,
                                      @Field("provinsi_id") String prov,
                                      @Field("kabupaten_id") String kot);

    @FormUrlEncoded
    @POST("getSchoolDLL")
    Call<GetSekolahResponse> getSekolah(@Field("type") String name,
                                        @Field("provinsi_id") String prov,
                                        @Field("kabupaten_id") String kot,
                                        @Field("kecamatan_id") String kec);

    @FormUrlEncoded
    @POST("CheckForgotPassword")
    Call<ServerResponse> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("update_fcm/access_token/{access_token}")
    Call<ServerResponse> sendFCM(@Field("id_user") String id_user,
                                 @Field("fcm_token") String fcm_token,
                                 @Path("access_token") String token);

    @FormUrlEncoded
    @POST("resendEmailActivation")
    Call<ServerResponse> resendActivation(@Field("email") String email);

    @FormUrlEncoded
    @POST("profileUser/access_token/{access_token}")
    Call<ProfileResponse> myProfile(@Field("id_user") String iduser,
                                    @Path("access_token") String token);

    @FormUrlEncoded
    @POST("checkChooseTeacher/access_token/{access_token}")
    Call<CheckTutorResponse> checkMyTutor(@Field("id_user") String iduser,
                                          @Path("access_token") String token);

    @FormUrlEncoded
    @POST("FollowTeacher/access_token/{access_token}")
    Call<ServerResponse> followTutor(@Path("access_token") String token,
                                     @Field("id_user") String iduser,
                                     @Field("id_teacher") String id_teacher);

    @FormUrlEncoded
    @POST("checkVersion/access_token/{access_token}")
    Call<CheckVersionResponse> checkVersion(@Path("access_token") String token,
                                            @Field("app") String iduser);

    @FormUrlEncoded
    @POST("setEmail/access_token/{access_token}")
    Call<ServerResponse> setEmail(@Path("access_token") String token,
                                  @Field("id") String iduser,
                                  @Field("email") String email);

    @FormUrlEncoded
    @POST("Profile_ChangeData/access_token/{access_token}")
    Call<ServerResponse> setProfile(@Path("access_token") String token,
                                  @Field("id") String iduser,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("no_hp") String no_hp);

    @FormUrlEncoded
    @POST("Profile_ChangePassword/access_token/{access_token}")
    Call<ServerResponse> setPassword(@Path("access_token") String token,
                                  @Field("id") String iduser,
                                  @Field("old_pass") String old_pass,
                                  @Field("new_pass") String new_pass);

    @FormUrlEncoded
    @POST("updatePhotoProfile/access_token/{access_token}")
    Call<ServerResponse> updatePhotoProfile(@Path("access_token") String token,
                                            @Field("id_user") String id_user,
                                            @Field("image") String image);

    @FormUrlEncoded
    @POST("List_Peserta_byFollowGuru/access_token/{access_token}")
    Call<ListPesertaResponse> getTotalStudent(@Path("access_token") String token,
                                                @Field("id_teacher") String id_teacher);

    @GET("ListTopUpload_Mengaji/access_token/{access_token}/data/{jum}")
    Call<ListTopMengaji> getTopMengaji(@Path("access_token") String token,
                                       @Path("jum") String sJumData);

    //CHAT

    @FormUrlEncoded
    @POST("user/create")
    Call<UserCreateResponse> chatCreate(@Field("id") String iduser,
                                        @Field("first_name") String fname,
                                        @Field("last_name") String lname);

    @FormUrlEncoded
    @POST("user/login")
    Call<ChatLoginResponse> chatLogin(@Field("id") String iduser);

    @FormUrlEncoded
    @POST("group/create")
    Call<ChatGroupCreateResponse> chatCreateGroup(@Field("name") String name, @Field("id") String id);

    @FormUrlEncoded
    @POST("group/add_user")
    Call<ChatAddUserResponse> chatAddtoGroup(@Field("id_group") String id_group, @Field("id_user") String id);

    @FormUrlEncoded
    @POST("group/send")
    Call<ChatSendMsgResp> chatSendMsg(@Field("id_group") String id_group, @Field("id_user") String id,
                                      @Field("message") String msg);

    @FormUrlEncoded
    @POST("group/receive")
    Call<ChatReceiveMsg> chatReceiveMsg(@Field("id_group") String id_group);

    @FormUrlEncoded
    @POST("group/unread")
    Call<ChatUnReadResponse> chatUnreadMsg(@Field("id_user") String id_user);

    @FormUrlEncoded
    @POST("group/unread")
    Call<ChatUnreadMsg> chatUnreadMsg(@Field("name_group")String name_group,
                                      @Field("id_user")String id_user);
    @FormUrlEncoded
    @POST("group/read")
    Call<ChatSendMsgResp> chatReadMsg(@Field("id_user") String id_user, @Field("id_group") String id_group);

}