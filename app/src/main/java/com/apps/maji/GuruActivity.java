package com.apps.maji;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.Adapter.GuruAdapter;
import com.apps.maji.Adapter.ListStudentAdapter;
import com.apps.maji.Fragment.MoreFragment;
import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataGuru;
import com.apps.maji.Model.Datum;
import com.apps.maji.Model.DetailTeacher;
import com.apps.maji.Model.ListGuruResponse;
import com.apps.maji.Model.ListPesertaResponse;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Utils.MyApplication;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.MyApplication.mAPIService;
import static com.apps.maji.Utils.MyApplication.sessionManager;

public class GuruActivity extends AppCompatActivity {

    private static final String TAG = "GuruActivity";
    TextView tvNama, tvEmail, tvTotal;
    ImageView img;
    DetailTeacher detailTeacher;
    GuruAdapter guruAdapter;
    DataGuru dataGuru;
    AlertDialog alertDialog;
    View llGuru;
    public Data myData;
    List<Datum> datumList;
    RecyclerView recyclerView;
    TextView tvNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guru);
        setTitle("Guru Saya");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String d = sessionManager.getData();
        myData = Data.create(d);
        initView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initView() {
        tvNama = findViewById(R.id.tvNamaGuru);
        tvEmail = findViewById(R.id.tvEmailGuru);
        tvTotal = findViewById(R.id.tvTotalSiswa);

        img = findViewById(R.id.imgGuru);
        recyclerView = findViewById(R.id.rvStudent);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        tvNo = findViewById(R.id.tvnodata);
        setTutor();
    }

    void setTutor() {
        String dt = MyApplication.sessionManager.getDataMyTutor();
        if (dt != null) {
            detailTeacher = DetailTeacher.create(dt);
            tvEmail.setText(detailTeacher.getEmail());
            if (tvNama != null)
                tvNama.setText(detailTeacher.getName());
            if (img != null)
                GlideApp.with(this)
                        .load(detailTeacher.getUserImage())
                        .apply(RequestOptions.circleCropTransform())
                        .into(img);
            getMyStudent();
        }
    }

    public void getMyStudent() {
        mAPIService.getTotalStudent(sessionManager.getToken(), detailTeacher.getId()).enqueue(new Callback<ListPesertaResponse>() {
            @Override
            public void onResponse(Call<ListPesertaResponse> call, Response<ListPesertaResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            if (response.body().getData().size() > 0) {
                                Log.i(TAG, "onResponse: ASD "+response.body().getData().toString());
                                tvTotal.setText(response.body().getData().size() + " Siswa");
                                datumList = response.body().getData();
                                ListStudentAdapter listStudentAdapter = new ListStudentAdapter(GuruActivity.this, datumList, new ListStudentAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(Datum item) {

                                    }
                                });
                                recyclerView.setAdapter(listStudentAdapter);
                            } else {
                                noData();
                            }
                        } else {
                            noData();
                        }
                    } else {
                        noData();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ListPesertaResponse> call, Throwable t) {

                Log.e(TAG, "onFailure:getMyStudent " + t.getMessage());
                tvNo.setVisibility(View.VISIBLE);
            }
        });
    }

    void noData(){
        tvTotal.setText("0 Siswa");
        tvNo.setVisibility(View.VISIBLE);
    }

    public void showCustomDialog(View v) {
        getGuru();
//        final Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_teacher);
//        dialog.setCancelable(true);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        ((TextView) dialog.findViewById(R.id.title)).setText(detailTeacher.getName());
////        ((TextView) dialog.findViewById(R.id.content)).setText(detailTeacher.getName());
//        llGuru = dialog.findViewById(R.id.llGuru);
//        llGuru.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getGuru();
//                dialog.dismiss();
//            }
//        });
//        GlideApp.with(this)
//                .load(detailTeacher.getUserImage())
//                .apply(RequestOptions.circleCropTransform())
//                .into(((CircleImageView) dialog.findViewById(R.id.image)));
//
//        ((ImageButton) dialog.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
    }

    public void getGuru() {
        mAPIService.getGuru(sessionManager.getToken()).enqueue(new Callback<ListGuruResponse>() {
            @Override
            public void onResponse(Call<ListGuruResponse> call, Response<ListGuruResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    if (response.body().isStatus()) {
                        if (response.body().getDataGuru().size() > 0) {

                            showSpinerDialog(response.body().getDataGuru());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListGuruResponse> call, Throwable t) {

                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
            }
        });
    }

    public void showSpinerDialog(List<DataGuru> dataGurus) {

        android.app.AlertDialog.Builder adb = new AlertDialog.Builder(this);
        View v = this.getLayoutInflater().inflate(R.layout.dialog_choose_tutor, null);
        TextView rippleViewClose = v.findViewById(R.id.close);

        final EditText searchBox = v.findViewById(R.id.searchBox);

        guruAdapter = new GuruAdapter(this, dataGurus, new GuruAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DataGuru item) {
                showDialog(item.getName(), item.getId());
                dataGuru = item;
            }

            @Override
            public void onItemClick(DataGuru item, String w) {

            }
        });

        RecyclerView recyclerView = v.findViewById(R.id.rvTutorChoose);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(guruAdapter);

        adb.setView(v);
        alertDialog = adb.create();
        alertDialog.setCancelable(true);


        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                guruAdapter.getFilter().filter(searchBox.getText().toString());
            }
        });

        rippleViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Log.e(TAG, "onClick: CLICK");
            }
        });
        alertDialog.show();
    }

    public void showDialog(final String str, final String id) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Apakah kamu ingin memilih " + str + " sebagai guru Anda?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                followTutor(id);
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    void followTutor(String idt) {
        mAPIService.followTutor(sessionManager.getToken(), myData.getId(), idt).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.d("Response", "= " + response.code());
                Log.d("Response", "= " + response.message());
                if (response.body() != null) {
                    if (response.body().isSuccess()) {
                        String serialize = dataGuru.serialize();
                        sessionManager.createDataMyTutor(serialize);
                        showDialogs(response.body().getMessage());
                        setTutor();
                    } else {

                        showDialogss(response.body().getMessage());
                    }
                } else
                    showDialogss(response.body().getMessage());
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e("failure", "message = " + t.getMessage());
                Log.e("failure", "cause = " + t.getCause());
                showDialogsz("Terjadi kesalahan, silahkan coba lagi");
            }
        });
    }

    public void showDialogs(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        builder.show();
    }

    public void showDialogss(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogsz(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getGuru();
            }
        });
        builder.show();
    }
}
