package com.apps.maji;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.apps.maji.Model.CheckVersionResponse;
import com.apps.maji.Rest.ApiConfig;
import com.apps.maji.Rest.AppConfig;
import com.apps.maji.Utils.Method;
import com.apps.maji.Utils.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;

    public SessionManager sessionManager;
    ApiConfig restApi;
    TextView tvVersion;
    String sVersionApp, sVersion, sToken = "", sForce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(this);
        restApi = AppConfig.getRetrofit().create(ApiConfig.class);
        tvVersion = findViewById(R.id.tvVersion);
        tvVersion.setText("Versi " + Method.checkVersion(this));
        sToken = sessionManager.getToken();
        initFCM();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkVersion();
    }

    void toMain() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    void initFCM(){
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        // [START retrieve_current_token]
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        sessionManager.createTokenFCM(token);
                        Log.d(TAG, msg);
                    }
                });
        // [END retrieve_current_token]
    }

    void checkVersion() {
        if (sToken.equals("")) {
            toMain();
        } else
            restApi.checkVersion(sToken, "student").enqueue(new Callback<CheckVersionResponse>() {
                @Override
                public void onResponse(Call<CheckVersionResponse> call, Response<CheckVersionResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isStatus()) {
                            sForce = response.body().getDataVersion().getForce();

                            sVersion = response.body().getDataVersion().getVersion();
                            sVersionApp = Method.checkVersion(SplashActivity.this);
                            int now = Integer.parseInt(sVersion.replace(".", "")), app = Integer.parseInt(sVersionApp.replace(".", ""));

                            if (now > app) {
                                if (sForce.equalsIgnoreCase("0")) {
                                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SplashActivity.this);
                                    // Message
                                    LayoutInflater layoutInflater = LayoutInflater.from(SplashActivity.this);
                                    View promptViewC = layoutInflater.inflate(R.layout.layout_message, null);

                                    TextView tvMsgTitle, tvMsgText;
                                    tvMsgTitle = (TextView) promptViewC.findViewById(R.id.tvMsgTitle);
                                    tvMsgText = (TextView) promptViewC.findViewById(R.id.tvMsgIsi);

                                    tvMsgTitle.setText(R.string.upgrade_messsage);

                                    tvMsgText.setVisibility(View.GONE);

                                    dialogBuilder.setCancelable(false);
                                    dialogBuilder.setView(promptViewC);
                                    dialogBuilder.setPositiveButton("OK", new
                                            DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int arg1) {
                                                    // TODO Auto-generated method stub

                                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    }
                                                }
                                            });
                                    dialogBuilder.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                            finish();
                                        }
                                    });
                                    AlertDialog alert11 = dialogBuilder.create();
                                    alert11.show();

                                    Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
                                    buttonbackground.setTextColor(Color.BLACK);

                                    Button buttonbackgrounds = alert11.getButton(DialogInterface.BUTTON_NEGATIVE);
                                    buttonbackgrounds.setTextColor(Color.BLACK);
                                } else {
                                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SplashActivity.this);
                                    // Message
                                    LayoutInflater layoutInflater = LayoutInflater.from(SplashActivity.this);
                                    View promptViewC = layoutInflater.inflate(R.layout.layout_message, null);

                                    TextView tvMsgTitle, tvMsgText;
                                    tvMsgTitle = promptViewC.findViewById(R.id.tvMsgTitle);
                                    tvMsgText = promptViewC.findViewById(R.id.tvMsgIsi);

                                    tvMsgTitle.setText(R.string.upgrade_messsage);

                                    tvMsgText.setVisibility(View.GONE);

                                    dialogBuilder.setCancelable(false);
                                    dialogBuilder.setView(promptViewC);
                                    dialogBuilder.setPositiveButton("OK", new
                                            DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int arg1) {
                                                    // TODO Auto-generated method stub

                                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    }
                                                }
                                            });
                                    AlertDialog alert11 = dialogBuilder.create();
                                    alert11.show();

                                    Button buttonbackground = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
                                    buttonbackground.setTextColor(Color.BLACK);

                                }
                            } else {
                                toMain();
                            }
                        } else
                            toMain();
                    } else
                        toMain();
                }

                @Override
                public void onFailure(Call<CheckVersionResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: checkVersion " + t.getMessage());

                    toMain();
                }
            });
    }
}
