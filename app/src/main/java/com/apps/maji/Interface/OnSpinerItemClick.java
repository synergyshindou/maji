package com.apps.maji.Interface;

import android.view.View;

/**
 * Created by Md Farhan Raja on 2/23/2017.
 */

public interface OnSpinerItemClick
{
    public void onClick(String item, int position);
    public void onTextClick(View v);
}
