package com.apps.maji;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.maji.Model.Data;
import com.apps.maji.Model.DataProfile;
import com.apps.maji.Model.ServerResponse;
import com.apps.maji.Utils.MyApplication;
import com.bumptech.glide.request.RequestOptions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.maji.Utils.MyApplication.mAPIService;
import static com.apps.maji.Utils.MyApplication.sessionManager;

public class ChangeProfileActivity extends AppCompatActivity {

    private static final String TAG = "ChangeProfileActivity";
    ImageView images;
    TextInputEditText etNama, etEmail, etHP;
    Button btnSave;
    public Data myData;
    Uri resultUri;
    android.app.AlertDialog b;
    android.app.AlertDialog.Builder dialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile);
        setTitle("Ubah Profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String d = sessionManager.getData();
        myData = Data.create(d);
        initView();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initView(){
        images = findViewById(R.id.imgProfil);
        etNama = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmail);
        etHP = findViewById(R.id.etHP);

        String dsp = MyApplication.sessionManager.getDataProfile();
        DataProfile dp = DataProfile.create(dsp);
        etNama.setText(dp.getName());
        etEmail.setText(dp.getEmail());
        etHP.setText(dp.getNoHp());

        int im;
        if (dp.getJk().equals("L"))
            im = R.drawable.icon_man;
        else
            im = R.drawable.icon_girl;

        GlideApp.with(this)
                .load(dp.getUserImage())
                .apply(RequestOptions.circleCropTransform())
                .placeholder(im)
                .into(images);

        btnSave = findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    void saveData(){
        if (etNama.getText().length() == 0){
            etNama.setError("Mohon di isi!");
            etNama.requestFocus();

        } else if (etEmail.getText().length() == 0){
            etEmail.setError("Mohon di isi!");
            etEmail.requestFocus();

        } else if (etHP.getText().length() == 0){
            etHP.setError("Mohon di isi!");
            etHP.requestFocus();

        } else {
            mAPIService.setProfile(sessionManager.getToken(), myData.getId(), etNama.getText().toString(), etEmail.getText().toString(), etHP.getText().toString()).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            if (response.body().isSuccess()){
                                showDialogs(response.body().getMessage());
                            } else {
                                showDialogss(response.body().getMessage());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e("failure", "message = " + t.getMessage());
                    Log.e("failure", "cause = " + t.getCause());
                    showDialogss("Terjadi kesalahan, silahkan coba lagi");
                }
            });
        }
    }

    public void showDialogss(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogs(String str) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    public void doChangeImage(View v){
        CropImage.activity()
                .setCropMenuCropButtonTitle("Done")
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(false)
                .setRequestedSize(512, 512)
                .setAspectRatio(1, 1)
                .start(ChangeProfileActivity.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                images.setImageURI(resultUri);
                BitmapDrawable drawable = (BitmapDrawable) images.getDrawable();

                Bitmap myBitmap;
                myBitmap = drawable.getBitmap();

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                byte[] ba = bytes.toByteArray();
                String b64 = Base64.encodeToString(ba, Base64.NO_WRAP);
                ShowProgressDialog();
                updateProfileImage(b64);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    void updateProfileImage(String b64){
        mAPIService.updatePhotoProfile(sessionManager.getToken(), myData.getId(), b64).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());
                    if (response.body().isSuccess()) {
                        Toast.makeText(ChangeProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        images.setImageURI(resultUri);
                    } else
                        Toast.makeText(ChangeProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                HideProgressDialog();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(ChangeProfileActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                HideProgressDialog();
            }
        });
    }


    public void ShowProgressDialog() {
        dialogBuilder = new android.app.AlertDialog.Builder(ChangeProfileActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        TextView tv = dialogView.findViewById(R.id.textViewDialog);
        tv.setText("Mengunduh Gambar. . .");
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }

}
